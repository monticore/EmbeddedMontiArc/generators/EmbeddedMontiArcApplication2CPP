/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarc.generator;

import de.monticore.lang.embeddedmontiarc.AbstractSymtab;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.generator.cpp.GeneratorCPP;
import de.monticore.lang.tagging._symboltable.TaggingResolver;
import de.se_rwth.commons.logging.Log;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;

import static de.monticore.lang.embeddedmontiarc.AbstractSymtab.createSymTabAndTaggingResolver;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class GenerationTest extends AbstractSymtab {
    @BeforeClass
    public static void init() {
        Log.enableFailQuick(false);
    }

    @Before
    public void clearLog() {
        Log.getFindings().clear();
    }

    @Test
    public void testWholePacmanGeneration() throws IOException {
        TaggingResolver symtab = createSymTabAndTaggingResolver("src/test/resources");

        ExpandedComponentInstanceSymbol componentSymbol = symtab.<ExpandedComponentInstanceSymbol>resolve("test.pacmanSampleGame", ExpandedComponentInstanceSymbol.KIND).orElse(null);
        assertNotNull(componentSymbol);
        EMAApplication2CPPGenerator generatorCPP = new EMAApplication2CPPGenerator();
        generatorCPP.useArmadilloBackend();
        //generatorCPP.setUseAlgebraicOptimizations(true);
        generatorCPP.setGenerationTargetPath("./target/generated-sources-cpp/generatortest/l1");
        List<File> files = generatorCPP.generateFiles(componentSymbol, symtab);
        testFilesAreEqual(files, "");

    }

    @Test
    public void testVideoMain1() throws IOException {
        TaggingResolver symtab = createSymTabAndTaggingResolver("src/test/resources");

        ExpandedComponentInstanceSymbol componentSymbol = symtab.<ExpandedComponentInstanceSymbol>resolve("test.videoMain1", ExpandedComponentInstanceSymbol.KIND).orElse(null);
        assertNotNull(componentSymbol);
        EMAApplication2CPPGenerator generatorCPP = new EMAApplication2CPPGenerator();
        generatorCPP.useArmadilloBackend();
        //generatorCPP.setUseAlgebraicOptimizations(true);
        generatorCPP.setGenerationTargetPath("./target/generated-sources-cpp/generatortest/video1");
        List<File> files = generatorCPP.generateFiles(componentSymbol, symtab);
        testFilesAreEqual(files, "video1/");

    }


    @Ignore
    @Test
    public void testSampleMapGeneration() throws IOException {
        //System.out.println(MapGenerator.generateTileSprites("wall", "pacwall", "defaultStage", 0, 17, 0, 1, 16, 16));
        //System.out.println(MapGenerator.generateTileSprites("wall", "pacwall", "defaultStage", 0, 1, 1, 17, 16, 16));
        //System.out.println(MapGenerator.generateTileSprites("wall", "pacwall", "defaultStage", 1, 17, 16, 17, 16, 16));
        //System.out.println(MapGenerator.generateTileSprites("wall", "pacwall", "defaultStage", 16, 17, 1, 16, 16, 16));


        int[] positionsX = {1
                , 2, 2, 2, 2, 2, 2, 2, 2, 2/* first row*/
                , 3, 3/*second*/
                , 4, 4, 4, 4, 4, 4, 4, 4, 4, 4/*third*/
                , 6, 6, 6, 6, 6, 6, 6, 6, 6/* fourth*/
                , 7, 7, 7, 7, 7, 7/* fifth */
                , 8, 8, 8, 8, 8, 8, 8/* sixth */
                , 9, 9, 9, 9, 9, 9/* seventh */
                , 10, 10, 10, 10, 10, 10, 10, 10, 10/* eighth */
                , 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12/* nineth */
                , 13, 13 /* tenth*/
                , 14, 14, 14, 14, 14, 14, 14, 14, 14, 14 /* eleventh*/
                , 15};
        int[] positionsY = {8
                , 2, 4, 5, 6, 8, 10, 12, 13, 14 /* first row */
                , 2, 10/* second */
                , 2, 4, 5, 6, 7, 8, 10, 12, 13, 14/* third */
                , 2, 4, 6, 7, 8, 10, 12, 13, 14/* fourth */
                , 2, 4, 6, 8, 10, 14/* fifth */
                , 2, 4, 8, 10, 11, 12, 14/* sixth */
                , 2, 4, 6, 8, 10, 14/* seventh */
                , 2, 4, 6, 7, 8, 10, 12, 13, 14/* eighth */
                , 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 14/* nineth */
                , 2, 14/* tenth */
                , 2, 4, 5, 6, 7, 8, 9, 10, 12, 14 /*eleventh*/
                , 12
        };
        int[][] tiles = new int[15][15];//ignore wall
        for (int i = 0; i < tiles.length; ++i) {
            for (int j = 0; j < tiles[0].length; ++j) {
                tiles[i][j] = 2;//pacdot
            }
        }

        for (int i = 0; i < positionsX.length; ++i) {
            tiles[positionsX[i] - 1][positionsY[i] - 1] = 1;//wall
        }
        for (int i = 0; i < tiles.length; ++i) {
            for (int j = 0; j < tiles[0].length; ++j) {
                String textureName = "pacdot";
                if (tiles[i][j] == 1) {
                    textureName = "pacwall";
                }
                System.out.println(MapGenerator.generateTileSprite("sprite" + "X" + (i + 1) + "Y" + (j + 1), i + 1, j + 1,
                        textureName, 48, 48, "defaultStage"));
            }
        }
        for (int i = 0; i < tiles.length; ++i) {
            for (int j = 0; j < tiles[0].length; ++j) {
                System.out.println("world(" + (i + 2) + ", " + (j + 2) + ") = " + tiles[i][j] + ";");
            }
        }
    }
}

