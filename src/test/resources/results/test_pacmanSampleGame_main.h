/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN
#define TEST_PACMANSAMPLEGAME_MAIN
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "test_pacmanSampleGame_main_needsInit.h"
#include "test_pacmanSampleGame_main_pacmanInit.h"
#include "test_pacmanSampleGame_main_stateSaver.h"
#include "test_pacmanSampleGame_main_stateSaverGhost.h"
#include "test_pacmanSampleGame_main_pacmanInputControl.h"
#include "test_pacmanSampleGame_main_ghost1InputControl.h"
#include "test_pacmanSampleGame_main_ghost2InputControl.h"
#include "test_pacmanSampleGame_main_ghost3InputControl.h"
#include "test_pacmanSampleGame_main_ghost4InputControl.h"
#include "test_pacmanSampleGame_main_inputStateSaver.h"
#include "test_pacmanSampleGame_main_inputStateSaverGhost1.h"
#include "test_pacmanSampleGame_main_inputStateSaverGhost2.h"
#include "test_pacmanSampleGame_main_inputStateSaverGhost3.h"
#include "test_pacmanSampleGame_main_inputStateSaverGhost4.h"
#include "test_pacmanSampleGame_main_pacmanMovementController.h"
#include "test_pacmanSampleGame_main_ghostMovementController.h"
#include "test_pacmanSampleGame_main_stubby.h"
#include "test_pacmanSampleGame_main_worldState.h"
#include "test_pacmanSampleGame_main_pacmanWorldManager.h"
#include "test_pacmanSampleGame_main_ghost1WorldManager.h"
#include "test_pacmanSampleGame_main_worldCreation.h"
#include "test_pacmanSampleGame_main_pacmanToTilePosition.h"
#include "test_pacmanSampleGame_main_ghost1ToTilePosition.h"
#include "test_pacmanSampleGame_main_ghost2ToTilePosition.h"
#include "test_pacmanSampleGame_main_ghost3ToTilePosition.h"
#include "test_pacmanSampleGame_main_ghost4ToTilePosition.h"
#include "test_pacmanSampleGame_main_pacmanGhostCollisionChecker.h"
using namespace arma;
class test_pacmanSampleGame_main{
public:
test_pacmanSampleGame_main_needsInit needsInit;
test_pacmanSampleGame_main_pacmanInit pacmanInit;
test_pacmanSampleGame_main_stateSaver stateSaver;
test_pacmanSampleGame_main_stateSaverGhost stateSaverGhost;
test_pacmanSampleGame_main_pacmanInputControl pacmanInputControl;
test_pacmanSampleGame_main_ghost1InputControl ghost1InputControl;
test_pacmanSampleGame_main_ghost2InputControl ghost2InputControl;
test_pacmanSampleGame_main_ghost3InputControl ghost3InputControl;
test_pacmanSampleGame_main_ghost4InputControl ghost4InputControl;
test_pacmanSampleGame_main_inputStateSaver inputStateSaver;
test_pacmanSampleGame_main_inputStateSaverGhost1 inputStateSaverGhost1;
test_pacmanSampleGame_main_inputStateSaverGhost2 inputStateSaverGhost2;
test_pacmanSampleGame_main_inputStateSaverGhost3 inputStateSaverGhost3;
test_pacmanSampleGame_main_inputStateSaverGhost4 inputStateSaverGhost4;
test_pacmanSampleGame_main_pacmanMovementController pacmanMovementController;
test_pacmanSampleGame_main_ghostMovementController ghostMovementController;
test_pacmanSampleGame_main_stubby stubby;
test_pacmanSampleGame_main_worldState worldState;
test_pacmanSampleGame_main_pacmanWorldManager pacmanWorldManager;
test_pacmanSampleGame_main_ghost1WorldManager ghost1WorldManager;
test_pacmanSampleGame_main_worldCreation worldCreation;
test_pacmanSampleGame_main_pacmanToTilePosition pacmanToTilePosition;
test_pacmanSampleGame_main_ghost1ToTilePosition ghost1ToTilePosition;
test_pacmanSampleGame_main_ghost2ToTilePosition ghost2ToTilePosition;
test_pacmanSampleGame_main_ghost3ToTilePosition ghost3ToTilePosition;
test_pacmanSampleGame_main_ghost4ToTilePosition ghost4ToTilePosition;
test_pacmanSampleGame_main_pacmanGhostCollisionChecker pacmanGhostCollisionChecker;
void init()
{
needsInit.init();
pacmanInit.init();
stateSaver.init();
stateSaverGhost.init();
pacmanInputControl.init();
ghost1InputControl.init();
ghost2InputControl.init();
ghost3InputControl.init();
ghost4InputControl.init();
inputStateSaver.init();
inputStateSaverGhost1.init();
inputStateSaverGhost2.init();
inputStateSaverGhost3.init();
inputStateSaverGhost4.init();
pacmanMovementController.init();
ghostMovementController.init();
stubby.init();
worldState.init();
pacmanWorldManager.init();
ghost1WorldManager.init();
worldCreation.init();
pacmanToTilePosition.init();
ghost1ToTilePosition.init();
ghost2ToTilePosition.init();
ghost3ToTilePosition.init();
ghost4ToTilePosition.init();
pacmanGhostCollisionChecker.init();
}
void execute()
{
pacmanInputControl.execute();
ghost1InputControl.execute();
ghost2InputControl.execute();
ghost3InputControl.execute();
ghost4InputControl.execute();
needsInit.reset = pacmanGhostCollisionChecker.pacmanDies;
needsInit.execute();
pacmanInit.exec = needsInit.out1;
pacmanInit.execute();
stateSaver.needsInit = needsInit.out1;
stateSaver.positionX = pacmanWorldManager.pacmanNextPositionX;
stateSaver.positionY = pacmanWorldManager.pacmanNextPositionY;
stateSaver.pacmanPositionX = pacmanWorldManager.pacmanNextPositionX;
stateSaver.pacmanPositionY = pacmanWorldManager.pacmanNextPositionY;
stateSaverGhost.needsInit = needsInit.out1;
stateSaverGhost.ghost1PositionX = ghost1WorldManager.pacmanNextPositionX;
stateSaverGhost.ghost1PositionY = ghost1WorldManager.pacmanNextPositionY;
stateSaverGhost.execute();
stateSaver.execute();
inputStateSaver.needsInit = needsInit.out1;
inputStateSaver.moveDownInput = pacmanInputControl.moveDown;
inputStateSaver.moveLeftInput = pacmanInputControl.moveLeft;
inputStateSaver.moveRightInput = pacmanInputControl.moveRight;
inputStateSaver.moveUpInput = pacmanInputControl.moveUp;
inputStateSaver.canMove = pacmanWorldManager.canMove;
inputStateSaver.canMoveLastDirection = pacmanWorldManager.canMoveLastDirection;
inputStateSaverGhost1.needsInit = needsInit.out1;
inputStateSaverGhost1.moveDownInput = ghost1InputControl.moveDownP2;
inputStateSaverGhost1.moveLeftInput = ghost1InputControl.moveLeftP2;
inputStateSaverGhost1.moveRightInput = ghost1InputControl.moveRightP2;
inputStateSaverGhost1.moveUpInput = ghost1InputControl.moveUpP2;
inputStateSaverGhost1.canMove = ghost1WorldManager.canMove;
inputStateSaverGhost1.canMoveLastDirection = ghost1WorldManager.canMoveLastDirection;
inputStateSaverGhost1.execute();
inputStateSaverGhost2.needsInit = needsInit.out1;
inputStateSaverGhost2.moveDownInput = ghost2InputControl.moveUp;
inputStateSaverGhost2.moveLeftInput = ghost2InputControl.moveDown;
inputStateSaverGhost2.moveRightInput = ghost2InputControl.moveLeft;
inputStateSaverGhost2.moveUpInput = ghost2InputControl.moveRight;
inputStateSaverGhost2.execute();
inputStateSaverGhost3.needsInit = needsInit.out1;
inputStateSaverGhost3.moveDownInput = ghost3InputControl.moveRight;
inputStateSaverGhost3.moveLeftInput = ghost3InputControl.moveUp;
inputStateSaverGhost3.moveRightInput = ghost3InputControl.moveDown;
inputStateSaverGhost3.moveUpInput = ghost3InputControl.moveLeft;
inputStateSaverGhost3.execute();
inputStateSaverGhost4.needsInit = needsInit.out1;
inputStateSaverGhost4.moveDownInput = ghost4InputControl.moveLeft;
inputStateSaverGhost4.moveLeftInput = ghost4InputControl.moveRight;
inputStateSaverGhost4.moveRightInput = ghost4InputControl.moveUp;
inputStateSaverGhost4.moveUpInput = ghost4InputControl.moveDown;
inputStateSaverGhost4.execute();
inputStateSaver.execute();
pacmanMovementController.initDone = pacmanInit.initDone;
pacmanMovementController.pacmanPositionX = stateSaver.pacmanPositionX;
pacmanMovementController.pacmanPositionY = stateSaver.pacmanPositionY;
pacmanMovementController.execute();
ghostMovementController.initDone = pacmanInit.initDone;
ghostMovementController.ghost1PositionX = stateSaverGhost.ghost1PositionX;
ghostMovementController.ghost1PositionY = stateSaverGhost.ghost1PositionY;
ghostMovementController.ghost2PositionX = stateSaverGhost.ghost2PositionX;
ghostMovementController.ghost2PositionY = stateSaverGhost.ghost2PositionY;
ghostMovementController.ghost3PositionX = stateSaverGhost.ghost3PositionX;
ghostMovementController.ghost3PositionY = stateSaverGhost.ghost3PositionY;
ghostMovementController.ghost4PositionX = stateSaverGhost.ghost4PositionX;
ghostMovementController.ghost4PositionY = stateSaverGhost.ghost4PositionY;
ghostMovementController.execute();
stubby.moveDown = inputStateSaver.moveDown;
stubby.moveUp = inputStateSaver.moveUp;
stubby.moveLeft = inputStateSaver.moveLeft;
stubby.moveRight = inputStateSaver.moveRight;
stubby.moveDownLastDirection = inputStateSaver.moveDownLastDirection;
stubby.moveUpLastDirection = inputStateSaver.moveUpLastDirection;
stubby.moveLeftLastDirection = inputStateSaver.moveLeftLastDirection;
stubby.moveRightLastDirection = inputStateSaver.moveRightLastDirection;
stubby.execute();
worldState.needsInit = needsInit.out1;
worldState.execute();
pacmanWorldManager.needsInit = needsInit.out1;
pacmanWorldManager.world = worldState.world;
pacmanWorldManager.moveDown = inputStateSaver.moveDown;
pacmanWorldManager.moveUp = inputStateSaver.moveUp;
pacmanWorldManager.moveLeft = inputStateSaver.moveLeft;
pacmanWorldManager.moveRight = inputStateSaver.moveRight;
pacmanWorldManager.moveDownLastDirection = inputStateSaver.moveDownLastDirection;
pacmanWorldManager.moveUpLastDirection = inputStateSaver.moveUpLastDirection;
pacmanWorldManager.moveLeftLastDirection = inputStateSaver.moveLeftLastDirection;
pacmanWorldManager.moveRightLastDirection = inputStateSaver.moveRightLastDirection;
pacmanWorldManager.pacmanPositionX = stateSaver.pacmanPositionX;
pacmanWorldManager.pacmanPositionY = stateSaver.pacmanPositionY;
pacmanWorldManager.execute();
ghost1WorldManager.needsInit = needsInit.out1;
ghost1WorldManager.world = worldState.world;
ghost1WorldManager.moveDown = inputStateSaverGhost1.moveDown;
ghost1WorldManager.moveUp = inputStateSaverGhost1.moveUp;
ghost1WorldManager.moveLeft = inputStateSaverGhost1.moveLeft;
ghost1WorldManager.moveRight = inputStateSaverGhost1.moveRight;
ghost1WorldManager.moveDownLastDirection = inputStateSaverGhost1.moveDownLastDirection;
ghost1WorldManager.moveUpLastDirection = inputStateSaverGhost1.moveUpLastDirection;
ghost1WorldManager.moveLeftLastDirection = inputStateSaverGhost1.moveLeftLastDirection;
ghost1WorldManager.moveRightLastDirection = inputStateSaverGhost1.moveRightLastDirection;
ghost1WorldManager.pacmanPositionX = stateSaverGhost.ghost1PositionXOut;
ghost1WorldManager.pacmanPositionY = stateSaverGhost.ghost1PositionYOut;
ghost1WorldManager.execute();
worldCreation.needsInit = needsInit.out1;
worldCreation.execute();
pacmanToTilePosition.positionX = stateSaver.pacmanPositionX;
pacmanToTilePosition.positionY = stateSaver.pacmanPositionY;
pacmanToTilePosition.execute();
ghost1ToTilePosition.positionX = stateSaverGhost.ghost1PositionXOut;
ghost1ToTilePosition.positionY = stateSaverGhost.ghost1PositionYOut;
ghost1ToTilePosition.execute();
ghost2ToTilePosition.positionX = stateSaverGhost.ghost2PositionXOut;
ghost2ToTilePosition.positionY = stateSaverGhost.ghost2PositionYOut;
ghost2ToTilePosition.execute();
ghost3ToTilePosition.positionX = stateSaverGhost.ghost3PositionXOut;
ghost3ToTilePosition.positionY = stateSaverGhost.ghost3PositionYOut;
ghost3ToTilePosition.execute();
ghost4ToTilePosition.positionX = stateSaverGhost.ghost4PositionXOut;
ghost4ToTilePosition.positionY = stateSaverGhost.ghost4PositionYOut;
ghost4ToTilePosition.execute();
pacmanGhostCollisionChecker.pacmanTilePositionX = pacmanToTilePosition.tilePositionX;
pacmanGhostCollisionChecker.pacmanTilePositionY = pacmanToTilePosition.tilePositionY;
pacmanGhostCollisionChecker.ghost1TilePositionX = ghost1ToTilePosition.tilePositionX;
pacmanGhostCollisionChecker.ghost1TilePositionY = ghost1ToTilePosition.tilePositionY;
pacmanGhostCollisionChecker.ghost2TilePositionX = ghost2ToTilePosition.tilePositionX;
pacmanGhostCollisionChecker.ghost2TilePositionY = ghost2ToTilePosition.tilePositionY;
pacmanGhostCollisionChecker.ghost3TilePositionX = ghost3ToTilePosition.tilePositionX;
pacmanGhostCollisionChecker.ghost3TilePositionY = ghost3ToTilePosition.tilePositionY;
pacmanGhostCollisionChecker.ghost4TilePositionX = ghost4ToTilePosition.tilePositionX;
pacmanGhostCollisionChecker.ghost4TilePositionY = ghost4ToTilePosition.tilePositionY;
pacmanGhostCollisionChecker.execute();
}

};
#endif
