/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_GHOST3WORLDMANAGER_PACMANWORLDHANDLER_WORLDMODIFIER
#define TEST_PACMANSAMPLEGAME_MAIN_GHOST3WORLDMANAGER_PACMANWORLDHANDLER_WORLDMODIFIER
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "test_pacmanSampleGame_main_ghost3WorldManager_pacmanWorldHandler_worldModifier_tilePosition.h"
#include "test_pacmanSampleGame_main_ghost3WorldManager_pacmanWorldHandler_worldModifier_pacDotDeleterDecider.h"
#include "test_pacmanSampleGame_main_ghost3WorldManager_pacmanWorldHandler_worldModifier_pacDotDeleter.h"
using namespace arma;
class test_pacmanSampleGame_main_ghost3WorldManager_pacmanWorldHandler_worldModifier{
const int tilesMaxX = 17;
const int tilesMaxY = 17;
const int tileSize = 16;
public:
bool isPacMan;
int pacmanPositionX;
int pacmanPositionY;
imat world;
test_pacmanSampleGame_main_ghost3WorldManager_pacmanWorldHandler_worldModifier_tilePosition tilePosition;
test_pacmanSampleGame_main_ghost3WorldManager_pacmanWorldHandler_worldModifier_pacDotDeleterDecider pacDotDeleterDecider;
test_pacmanSampleGame_main_ghost3WorldManager_pacmanWorldHandler_worldModifier_pacDotDeleter pacDotDeleter;
void init()
{
world=imat(tilesMaxX,tilesMaxY);
tilePosition.init();
pacDotDeleterDecider.init();
pacDotDeleter.init();
}
void execute()
{
tilePosition.positionX = pacmanPositionX;
tilePosition.positionY = pacmanPositionY;
tilePosition.execute();
pacDotDeleterDecider.world = world;
pacDotDeleterDecider.isPacMan = isPacMan;
pacDotDeleterDecider.tilePositionX = tilePosition.tilePositionX;
pacDotDeleterDecider.tilePositionY = tilePosition.tilePositionY;
pacDotDeleterDecider.execute();
pacDotDeleter.tilePositionX = tilePosition.tilePositionX;
pacDotDeleter.tilePositionY = tilePosition.tilePositionY;
pacDotDeleter.shouldDelete = pacDotDeleterDecider.shouldDelete;
pacDotDeleter.execute();
}

};
#endif
