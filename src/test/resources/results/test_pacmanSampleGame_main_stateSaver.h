/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_STATESAVER
#define TEST_PACMANSAMPLEGAME_MAIN_STATESAVER
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_pacmanSampleGame_main_stateSaver{
public:
bool needsInit;
int positionX;
int positionY;
int pacmanPositionX;
int pacmanPositionY;
int pacmanLastPositionX;
int pacmanLastPositionY;
void init()
{
}
void execute()
{
pacmanLastPositionX = pacmanPositionX;
pacmanLastPositionY = pacmanPositionY;
if((needsInit)){
pacmanPositionX = 336;
pacmanPositionY = 720;
pacmanLastPositionX = pacmanPositionX;
pacmanLastPositionY = pacmanPositionY;
}
else {
pacmanPositionX = positionX;
pacmanPositionY = positionY;
}
}

};
#endif
