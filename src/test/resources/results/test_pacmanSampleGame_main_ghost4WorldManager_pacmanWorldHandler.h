/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_GHOST4WORLDMANAGER_PACMANWORLDHANDLER
#define TEST_PACMANSAMPLEGAME_MAIN_GHOST4WORLDMANAGER_PACMANWORLDHANDLER
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "test_pacmanSampleGame_main_ghost4WorldManager_pacmanWorldHandler_movementHandler.h"
#include "test_pacmanSampleGame_main_ghost4WorldManager_pacmanWorldHandler_worldModifier.h"
using namespace arma;
class test_pacmanSampleGame_main_ghost4WorldManager_pacmanWorldHandler{
const int tilesMaxX = 17;
const int tilesMaxY = 17;
public:
bool needsInit;
bool moveUp;
bool moveDown;
bool moveLeft;
bool moveRight;
bool moveUpLastDirection;
bool moveDownLastDirection;
bool moveLeftLastDirection;
bool moveRightLastDirection;
int pacmanPositionX;
int pacmanPositionY;
imat world;
int pacmanNextPositionX;
int pacmanNextPositionY;
bool canMove;
bool canMoveLastDirection;
test_pacmanSampleGame_main_ghost4WorldManager_pacmanWorldHandler_movementHandler movementHandler;
test_pacmanSampleGame_main_ghost4WorldManager_pacmanWorldHandler_worldModifier worldModifier;
void init()
{
world=imat(tilesMaxX,tilesMaxY);
movementHandler.init();
worldModifier.init();
}
void execute()
{
movementHandler.moveUp = moveUp;
movementHandler.moveDown = moveDown;
movementHandler.moveLeft = moveLeft;
movementHandler.moveRight = moveRight;
movementHandler.moveUpLastDirection = moveUpLastDirection;
movementHandler.moveDownLastDirection = moveDownLastDirection;
movementHandler.moveLeftLastDirection = moveLeftLastDirection;
movementHandler.moveRightLastDirection = moveRightLastDirection;
movementHandler.pacmanPositionX = pacmanPositionX;
movementHandler.pacmanPositionY = pacmanPositionY;
movementHandler.world = world;
movementHandler.execute();
worldModifier.pacmanPositionX = pacmanPositionX;
worldModifier.pacmanPositionY = pacmanPositionY;
worldModifier.world = world;
worldModifier.execute();
pacmanNextPositionX = movementHandler.pacmanNextPositionX;
pacmanNextPositionY = movementHandler.pacmanNextPositionY;
canMove = movementHandler.canMove;
canMoveLastDirection = movementHandler.canMoveLastDirection;
}

};
#endif
