/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_PACMANGHOSTCOLLISIONCHECKER_COLLISIONCHECKER_1_
#define TEST_PACMANSAMPLEGAME_MAIN_PACMANGHOSTCOLLISIONCHECKER_COLLISIONCHECKER_1_
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_pacmanSampleGame_main_pacmanGhostCollisionChecker_collisionChecker_1_{
public:
int position1X;
int position1Y;
int position2X;
int position2Y;
bool collides;
void init()
{
}
void execute()
{
if(((position1X == position2X)&&(position1Y == position2Y))){
collides = true;
}
else {
collides = false;
}
}

};
#endif
