/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_STUBBY
#define TEST_PACMANSAMPLEGAME_MAIN_STUBBY
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_pacmanSampleGame_main_stubby{
public:
bool moveDown;
bool moveUp;
bool moveLeft;
bool moveRight;
bool moveDownLastDirection;
bool moveUpLastDirection;
bool moveLeftLastDirection;
bool moveRightLastDirection;
void init()
{
}
void execute()
{
}

};
#endif
