/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_GHOST2WORLDMANAGER_PACMANWORLDHANDLER_WORLDMODIFIER_TILEPOSITION
#define TEST_PACMANSAMPLEGAME_MAIN_GHOST2WORLDMANAGER_PACMANWORLDHANDLER_WORLDMODIFIER_TILEPOSITION
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_pacmanSampleGame_main_ghost2WorldManager_pacmanWorldHandler_worldModifier_tilePosition{
const int tileSize = 16;
public:
int positionX;
int positionY;
int tilePositionX;
int tilePositionY;
void init()
{
}
void execute()
{
tilePositionX = positionX/tileSize;
tilePositionY = positionY/tileSize;
if(((positionX%tileSize != 0))){
tilePositionX = tilePositionX+1;
}
if(((positionY%tileSize != 0))){
tilePositionY = tilePositionY+1;
}
}

};
#endif
