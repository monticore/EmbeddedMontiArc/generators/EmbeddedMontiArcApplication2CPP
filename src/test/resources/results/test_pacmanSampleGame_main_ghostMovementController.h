/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_GHOSTMOVEMENTCONTROLLER
#define TEST_PACMANSAMPLEGAME_MAIN_GHOSTMOVEMENTCONTROLLER
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "stdio.h"
#include "SDL.h"
#include "dcw/sys/HelperAPP.hpp"
#include "dcw/sys/TextureLoader.hpp"
#include "dcw/sys/Square2D.hpp"
using namespace arma;
class test_pacmanSampleGame_main_ghostMovementController{
public:
int ghost1PositionX;
int ghost1PositionY;
int ghost2PositionX;
int ghost2PositionY;
int ghost3PositionX;
int ghost3PositionY;
int ghost4PositionX;
int ghost4PositionY;
bool initDone;
void init()
{
}
void execute()
{
	HelperAPP::getSprite("ghost1")->position.x = ghost1PositionX;
	HelperAPP::getSprite("ghost1")->position.y = ghost1PositionY;
	HelperAPP::getSprite("ghost2")->position.x = ghost2PositionX;
	HelperAPP::getSprite("ghost2")->position.y = ghost2PositionY;
	HelperAPP::getSprite("ghost3")->position.x = ghost3PositionX;
	HelperAPP::getSprite("ghost3")->position.y = ghost3PositionY;
	HelperAPP::getSprite("ghost4")->position.x = ghost4PositionX;
	HelperAPP::getSprite("ghost4")->position.y = ghost4PositionY;
}

};
#endif
