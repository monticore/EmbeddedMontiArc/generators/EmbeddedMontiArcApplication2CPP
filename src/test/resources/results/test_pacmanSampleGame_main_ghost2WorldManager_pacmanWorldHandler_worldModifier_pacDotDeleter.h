/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_GHOST2WORLDMANAGER_PACMANWORLDHANDLER_WORLDMODIFIER_PACDOTDELETER
#define TEST_PACMANSAMPLEGAME_MAIN_GHOST2WORLDMANAGER_PACMANWORLDHANDLER_WORLDMODIFIER_PACDOTDELETER
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "stdio.h"
#include "SDL.h"
#include "dcw/sys/HelperAPP.hpp"
#include "dcw/sys/TextureLoader.hpp"
#include "dcw/sys/Square2D.hpp"
using namespace arma;
class test_pacmanSampleGame_main_ghost2WorldManager_pacmanWorldHandler_worldModifier_pacDotDeleter{
public:
int tilePositionX;
int tilePositionY;
bool shouldDelete;
void init()
{
}
void execute()
{
	if (shouldDelete) 
	{ 
		HelperAPP::removeSprite(std::string().append("sprite")
.append("X")
.append(std::to_string(tilePositionX))
.append("Y")
.append(std::to_string(tilePositionY))
);
	} 
}

};
#endif
