/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_PACMANWORLDMANAGER_PACMANWORLDHANDLER_WORLDMODIFIER_PACDOTDELETERDECIDER
#define TEST_PACMANSAMPLEGAME_MAIN_PACMANWORLDMANAGER_PACMANWORLDHANDLER_WORLDMODIFIER_PACDOTDELETERDECIDER
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_pacmanSampleGame_main_pacmanWorldManager_pacmanWorldHandler_worldModifier_pacDotDeleterDecider{
const int tilesMaxX = 17;
const int tilesMaxY = 17;
const int tileSize = 48;
public:
bool isPacMan;
int tilePositionX;
int tilePositionY;
imat world;
bool shouldDelete;
void init()
{
world=imat(tilesMaxX,tilesMaxY);
}
void execute()
{
if((isPacMan)){
if(((world(tilePositionX, tilePositionY) == 2))){
shouldDelete = 1;
}
else {
shouldDelete = 0;
}
}
}

};
#endif
