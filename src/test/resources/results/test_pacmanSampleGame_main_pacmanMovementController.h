/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_PACMANMOVEMENTCONTROLLER
#define TEST_PACMANSAMPLEGAME_MAIN_PACMANMOVEMENTCONTROLLER
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "stdio.h"
#include "SDL.h"
#include "dcw/sys/HelperAPP.hpp"
#include "dcw/sys/TextureLoader.hpp"
#include "dcw/sys/Square2D.hpp"
using namespace arma;
class test_pacmanSampleGame_main_pacmanMovementController{
public:
int pacmanPositionX;
int pacmanPositionY;
bool initDone;
void init()
{
}
void execute()
{
	HelperAPP::getSprite("pacman")->position.x = pacmanPositionX;
	HelperAPP::getSprite("pacman")->position.y = pacmanPositionY;
}

};
#endif
