/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_WORLDSTATE
#define TEST_PACMANSAMPLEGAME_MAIN_WORLDSTATE
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_pacmanSampleGame_main_worldState{
const int tilesMaxX = 17;
const int tilesMaxY = 17;
public:
bool needsInit;
bool needsReset;
imat world;
void init()
{
world=imat(tilesMaxX,tilesMaxY);
}
void execute()
{
if((needsInit)){
for( auto x=1;x<=tilesMaxX;++x){
for( auto y=1;y<=tilesMaxY;++y){
world(x-1, y-1) = 0;
}
}
for( auto x=1;x<=1;++x){
for( auto y=1;y<=tilesMaxY;++y){
world(x-1, y-1) = 1;
}
}
for( auto x=1;x<=tilesMaxX;++x){
for( auto y=1;y<=1;++y){
world(x-1, y-1) = 1;
}
}
for( auto x=tilesMaxX;x<=tilesMaxX;++x){
for( auto y=1;y<=tilesMaxY;++y){
world(x-1, y-1) = 1;
}
}
for( auto x=1;x<=tilesMaxX;++x){
for( auto y=tilesMaxY;y<=tilesMaxY;++y){
world(x-1, y-1) = 1;
}
}
world(2-1, 2-1) = 2;
world(2-1, 3-1) = 2;
world(2-1, 4-1) = 2;
world(2-1, 5-1) = 2;
world(2-1, 6-1) = 2;
world(2-1, 7-1) = 2;
world(2-1, 8-1) = 2;
world(2-1, 9-1) = 1;
world(2-1, 10-1) = 2;
world(2-1, 11-1) = 2;
world(2-1, 12-1) = 2;
world(2-1, 13-1) = 2;
world(2-1, 14-1) = 2;
world(2-1, 15-1) = 2;
world(2-1, 16-1) = 2;
world(3-1, 2-1) = 2;
world(3-1, 3-1) = 1;
world(3-1, 4-1) = 2;
world(3-1, 5-1) = 1;
world(3-1, 6-1) = 1;
world(3-1, 7-1) = 1;
world(3-1, 8-1) = 2;
world(3-1, 9-1) = 1;
world(3-1, 10-1) = 2;
world(3-1, 11-1) = 1;
world(3-1, 12-1) = 2;
world(3-1, 13-1) = 1;
world(3-1, 14-1) = 1;
world(3-1, 15-1) = 1;
world(3-1, 16-1) = 2;
world(4-1, 2-1) = 2;
world(4-1, 3-1) = 1;
world(4-1, 4-1) = 2;
world(4-1, 5-1) = 2;
world(4-1, 6-1) = 2;
world(4-1, 7-1) = 2;
world(4-1, 8-1) = 2;
world(4-1, 9-1) = 2;
world(4-1, 10-1) = 2;
world(4-1, 11-1) = 1;
world(4-1, 12-1) = 2;
world(4-1, 13-1) = 2;
world(4-1, 14-1) = 2;
world(4-1, 15-1) = 2;
world(4-1, 16-1) = 2;
world(5-1, 2-1) = 2;
world(5-1, 3-1) = 1;
world(5-1, 4-1) = 2;
world(5-1, 5-1) = 1;
world(5-1, 6-1) = 2;
world(5-1, 7-1) = 1;
world(5-1, 8-1) = 1;
world(5-1, 9-1) = 1;
world(5-1, 10-1) = 2;
world(5-1, 11-1) = 1;
world(5-1, 12-1) = 2;
world(5-1, 13-1) = 1;
world(5-1, 14-1) = 1;
world(5-1, 15-1) = 1;
world(5-1, 16-1) = 2;
world(6-1, 2-1) = 2;
world(6-1, 3-1) = 2;
world(6-1, 4-1) = 2;
world(6-1, 5-1) = 2;
world(6-1, 6-1) = 2;
world(6-1, 7-1) = 2;
world(6-1, 8-1) = 2;
world(6-1, 9-1) = 2;
world(6-1, 10-1) = 2;
world(6-1, 11-1) = 2;
world(6-1, 12-1) = 2;
world(6-1, 13-1) = 2;
world(6-1, 14-1) = 2;
world(6-1, 15-1) = 2;
world(6-1, 16-1) = 2;
world(7-1, 2-1) = 2;
world(7-1, 3-1) = 1;
world(7-1, 4-1) = 2;
world(7-1, 5-1) = 1;
world(7-1, 6-1) = 2;
world(7-1, 7-1) = 1;
world(7-1, 8-1) = 1;
world(7-1, 9-1) = 1;
world(7-1, 10-1) = 2;
world(7-1, 11-1) = 1;
world(7-1, 12-1) = 2;
world(7-1, 13-1) = 1;
world(7-1, 14-1) = 1;
world(7-1, 15-1) = 1;
world(7-1, 16-1) = 2;
world(8-1, 2-1) = 2;
world(8-1, 3-1) = 1;
world(8-1, 4-1) = 2;
world(8-1, 5-1) = 1;
world(8-1, 6-1) = 2;
world(8-1, 7-1) = 1;
world(8-1, 8-1) = 2;
world(8-1, 9-1) = 1;
world(8-1, 10-1) = 2;
world(8-1, 11-1) = 1;
world(8-1, 12-1) = 2;
world(8-1, 13-1) = 2;
world(8-1, 14-1) = 2;
world(8-1, 15-1) = 1;
world(8-1, 16-1) = 2;
world(9-1, 2-1) = 2;
world(9-1, 3-1) = 1;
world(9-1, 4-1) = 2;
world(9-1, 5-1) = 1;
world(9-1, 6-1) = 2;
world(9-1, 7-1) = 2;
world(9-1, 8-1) = 2;
world(9-1, 9-1) = 1;
world(9-1, 10-1) = 2;
world(9-1, 11-1) = 1;
world(9-1, 12-1) = 1;
world(9-1, 13-1) = 1;
world(9-1, 14-1) = 2;
world(9-1, 15-1) = 1;
world(9-1, 16-1) = 2;
world(10-1, 2-1) = 2;
world(10-1, 3-1) = 1;
world(10-1, 4-1) = 2;
world(10-1, 5-1) = 1;
world(10-1, 6-1) = 2;
world(10-1, 7-1) = 1;
world(10-1, 8-1) = 2;
world(10-1, 9-1) = 1;
world(10-1, 10-1) = 2;
world(10-1, 11-1) = 1;
world(10-1, 12-1) = 2;
world(10-1, 13-1) = 2;
world(10-1, 14-1) = 2;
world(10-1, 15-1) = 1;
world(10-1, 16-1) = 2;
world(11-1, 2-1) = 2;
world(11-1, 3-1) = 1;
world(11-1, 4-1) = 2;
world(11-1, 5-1) = 1;
world(11-1, 6-1) = 2;
world(11-1, 7-1) = 1;
world(11-1, 8-1) = 1;
world(11-1, 9-1) = 1;
world(11-1, 10-1) = 2;
world(11-1, 11-1) = 1;
world(11-1, 12-1) = 2;
world(11-1, 13-1) = 1;
world(11-1, 14-1) = 1;
world(11-1, 15-1) = 1;
world(11-1, 16-1) = 2;
world(12-1, 2-1) = 2;
world(12-1, 3-1) = 2;
world(12-1, 4-1) = 2;
world(12-1, 5-1) = 2;
world(12-1, 6-1) = 2;
world(12-1, 7-1) = 2;
world(12-1, 8-1) = 2;
world(12-1, 9-1) = 2;
world(12-1, 10-1) = 2;
world(12-1, 11-1) = 2;
world(12-1, 12-1) = 2;
world(12-1, 13-1) = 2;
world(12-1, 14-1) = 2;
world(12-1, 15-1) = 2;
world(12-1, 16-1) = 2;
world(13-1, 2-1) = 2;
world(13-1, 3-1) = 1;
world(13-1, 4-1) = 1;
world(13-1, 5-1) = 1;
world(13-1, 6-1) = 2;
world(13-1, 7-1) = 1;
world(13-1, 8-1) = 1;
world(13-1, 9-1) = 1;
world(13-1, 10-1) = 1;
world(13-1, 11-1) = 1;
world(13-1, 12-1) = 1;
world(13-1, 13-1) = 1;
world(13-1, 14-1) = 2;
world(13-1, 15-1) = 1;
world(13-1, 16-1) = 2;
world(14-1, 2-1) = 2;
world(14-1, 3-1) = 1;
world(14-1, 4-1) = 2;
world(14-1, 5-1) = 2;
world(14-1, 6-1) = 2;
world(14-1, 7-1) = 2;
world(14-1, 8-1) = 2;
world(14-1, 9-1) = 2;
world(14-1, 10-1) = 2;
world(14-1, 11-1) = 2;
world(14-1, 12-1) = 2;
world(14-1, 13-1) = 2;
world(14-1, 14-1) = 2;
world(14-1, 15-1) = 1;
world(14-1, 16-1) = 2;
world(15-1, 2-1) = 2;
world(15-1, 3-1) = 1;
world(15-1, 4-1) = 2;
world(15-1, 5-1) = 1;
world(15-1, 6-1) = 1;
world(15-1, 7-1) = 1;
world(15-1, 8-1) = 1;
world(15-1, 9-1) = 1;
world(15-1, 10-1) = 1;
world(15-1, 11-1) = 1;
world(15-1, 12-1) = 2;
world(15-1, 13-1) = 1;
world(15-1, 14-1) = 2;
world(15-1, 15-1) = 1;
world(15-1, 16-1) = 2;
world(16-1, 2-1) = 2;
world(16-1, 3-1) = 2;
world(16-1, 4-1) = 2;
world(16-1, 5-1) = 2;
world(16-1, 6-1) = 2;
world(16-1, 7-1) = 2;
world(16-1, 8-1) = 2;
world(16-1, 9-1) = 2;
world(16-1, 10-1) = 2;
world(16-1, 11-1) = 2;
world(16-1, 12-1) = 2;
world(16-1, 13-1) = 1;
world(16-1, 14-1) = 2;
world(16-1, 15-1) = 2;
world(16-1, 16-1) = 2;
}
}

};
#endif
