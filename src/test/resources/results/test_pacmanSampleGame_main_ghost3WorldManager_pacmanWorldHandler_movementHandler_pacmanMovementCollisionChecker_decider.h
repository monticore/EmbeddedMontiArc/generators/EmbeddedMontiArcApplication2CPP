/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_GHOST3WORLDMANAGER_PACMANWORLDHANDLER_MOVEMENTHANDLER_PACMANMOVEMENTCOLLISIONCHECKER_DECIDER
#define TEST_PACMANSAMPLEGAME_MAIN_GHOST3WORLDMANAGER_PACMANWORLDHANDLER_MOVEMENTHANDLER_PACMANMOVEMENTCOLLISIONCHECKER_DECIDER
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_pacmanSampleGame_main_ghost3WorldManager_pacmanWorldHandler_movementHandler_pacmanMovementCollisionChecker_decider{
public:
bool canMove;
bool canMoveLastDirection;
int pacmanPositionX;
int pacmanPositionY;
int pacmanChosenNextPositionXCurrentDirection;
int pacmanChosenNextPositionYCurrentDirection;
int pacmanChosenNextPositionXLastDirection;
int pacmanChosenNextPositionYLastDirection;
int pacmanChosenNextPositionX;
int pacmanChosenNextPositionY;
void init()
{
}
void execute()
{
bool updatedPosition = 0;
if((canMove)){
pacmanChosenNextPositionX = pacmanChosenNextPositionXCurrentDirection;
pacmanChosenNextPositionY = pacmanChosenNextPositionYCurrentDirection;
updatedPosition = 1;
}
if(((updatedPosition == 0))){
if((canMoveLastDirection)){
pacmanChosenNextPositionX = pacmanChosenNextPositionXLastDirection;
pacmanChosenNextPositionY = pacmanChosenNextPositionYLastDirection;
}
}
}

};
#endif
