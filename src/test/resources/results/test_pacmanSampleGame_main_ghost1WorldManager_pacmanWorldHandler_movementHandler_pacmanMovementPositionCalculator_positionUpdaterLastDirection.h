/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_GHOST1WORLDMANAGER_PACMANWORLDHANDLER_MOVEMENTHANDLER_PACMANMOVEMENTPOSITIONCALCULATOR_POSITIONUPDATERLASTDIRECTION
#define TEST_PACMANSAMPLEGAME_MAIN_GHOST1WORLDMANAGER_PACMANWORLDHANDLER_MOVEMENTHANDLER_PACMANMOVEMENTPOSITIONCALCULATOR_POSITIONUPDATERLASTDIRECTION
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_pacmanSampleGame_main_ghost1WorldManager_pacmanWorldHandler_movementHandler_pacmanMovementPositionCalculator_positionUpdaterLastDirection{
const int tilesMaxX = 17;
const int tilesMaxY = 17;
const int tileSize = 48;
public:
bool moveUp;
bool moveDown;
bool moveLeft;
bool moveRight;
int pacmanPositionX;
int pacmanPositionY;
int newPositionX;
int newPositionY;
void init()
{
}
void execute()
{
newPositionX = pacmanPositionX;
newPositionY = pacmanPositionY;
if((moveUp)){
if(((pacmanPositionY+1 < tilesMaxY*tileSize))){
newPositionY = newPositionY+1;
}
}
else if((moveDown)){
if(((pacmanPositionY-1 >= 0))){
newPositionY = newPositionY-1;
}
}
else if((moveLeft)){
if(((pacmanPositionX-1 >= 0))){
newPositionX = newPositionX-1;
}
}
else if((moveRight)){
if(((pacmanPositionX+1 < tilesMaxX*tileSize))){
newPositionX = newPositionX+1;
}
}
}

};
#endif
