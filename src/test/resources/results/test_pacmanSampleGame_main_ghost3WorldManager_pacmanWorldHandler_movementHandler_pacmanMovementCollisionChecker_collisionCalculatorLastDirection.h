/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_GHOST3WORLDMANAGER_PACMANWORLDHANDLER_MOVEMENTHANDLER_PACMANMOVEMENTCOLLISIONCHECKER_COLLISIONCALCULATORLASTDIRECTION
#define TEST_PACMANSAMPLEGAME_MAIN_GHOST3WORLDMANAGER_PACMANWORLDHANDLER_MOVEMENTHANDLER_PACMANMOVEMENTCOLLISIONCHECKER_COLLISIONCALCULATORLASTDIRECTION
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_pacmanSampleGame_main_ghost3WorldManager_pacmanWorldHandler_movementHandler_pacmanMovementCollisionChecker_collisionCalculatorLastDirection{
const int tilesMaxX = 17;
const int tilesMaxY = 17;
const int tileSize = 16;
public:
bool moveUp;
bool moveDown;
bool moveLeft;
bool moveRight;
imat world;
int pacmanPositionX;
int pacmanPositionY;
int pacmanPossibleNextPositionX;
int pacmanPossibleNextPositionY;
bool canMove;
int pacmanChosenNextPositionX;
int pacmanChosenNextPositionY;
void init()
{
world=imat(tilesMaxX,tilesMaxY);
}
void execute()
{
bool xEven = 0;
bool yEven = 0;
int indexX = (pacmanPossibleNextPositionX)/tileSize;
int indexY = (pacmanPossibleNextPositionY)/tileSize;
if((((pacmanPossibleNextPositionX)%tileSize != 0))){
indexX = indexX+1;
xEven = 0;
}
else {
xEven = 1;
}
if((((pacmanPossibleNextPositionY)%tileSize != 0))){
indexY = indexY+1;
yEven = 0;
}
else {
yEven = 1;
}
if(((indexX >= tilesMaxX))){
indexX = tilesMaxX-1;
}
if(((indexY >= tilesMaxY))){
indexY = tilesMaxY-1;
}
int downX = indexX;
int downY = indexY-1;
int upX = indexX;
int upY = indexY;
int rightX = indexX;
int rightY = indexY;
int leftX = indexX-1;
int leftY = indexY;
if(((downY < 0))){
downY = 0;
}
if(((leftX < 0))){
leftX = 1;
}
if(((upY >= tilesMaxY))){
upY = tilesMaxY-1;
}
if(((rightX >= tilesMaxX))){
rightX = tilesMaxX-1;
}
int rightX2 = rightX;
int rightY2 = rightY-1;
int leftX2 = leftX;
int leftY2 = leftY-1;
int downX2 = downX-1;
int downY2 = downY;
int upX2 = upX-1;
int upY2 = upY;
if((yEven)){
rightY2 = rightY;
leftY2 = leftY;
}
if((xEven)){
downX2 = downX;
upX2 = upX;
}
if(((rightY2 < 0))){
rightY2 = 0;
}
if(((downX2 < 0))){
downX2 = 0;
}
if(((leftY2 < 0))){
leftY2 = 0;
}
if(((downY2 < 0))){
downY2 = 0;
}
int value = world(indexX, indexY);
int valueDown = world(downX, downY);
int valueDown2 = world(downX2, downY2);
int valueRight = world(rightX, rightY);
int valueRight2 = world(rightX2, rightY2);
int valueUp = world(upX, upY);
int valueUp2 = world(upX2, upY2);
int valueLeft = world(leftX, leftY);
int valueLeft2 = world(leftX2, leftY2);
bool notSafePosition;
if((xEven&&yEven)){
notSafePosition = 0;
}
else {
notSafePosition = 1;
}
if((notSafePosition&&((value == 1)||((valueDown == 1)&&moveDown)||((valueDown2 == 1)&&moveDown)||((valueRight == 1)&&moveRight)||((valueRight2 == 1)&&moveRight)||((valueUp == 1)&&moveUp)||((valueUp2 == 1)&&moveUp)||((valueLeft == 1)&&moveLeft)||((valueLeft2 == 1)&&moveLeft)))){
canMove = 0;
pacmanChosenNextPositionX = pacmanPositionX;
pacmanChosenNextPositionY = pacmanPositionY;
}
else {
canMove = 1;
pacmanChosenNextPositionX = pacmanPossibleNextPositionX;
pacmanChosenNextPositionY = pacmanPossibleNextPositionY;
}
}

};
#endif
