/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_VIDEOMAIN1_MAIN
#define TEST_VIDEOMAIN1_MAIN
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "stdio.h"
#include "SDL.h"
#include "dcw/sys/HelperAPP.hpp"
#include "dcw/sys/TextureLoader.hpp"
#include "dcw/sys/Square2D.hpp"
using namespace arma;
class test_videoMain1_main{
public:
bool needsInit;
void init()
{
}
void execute()
{
	if (needsInit) 
	{ 
		HelperAPP::setWindowSize(500, 500);
		Square2D* pacman = new Square2D(HelperAPP::getShader());
		pacman->name = "pacman";
		HelperAPP::addSprite(pacman);
		HelperAPP::getSprite("pacman")->position.x = 200.0;
		HelperAPP::getSprite("pacman")->position.y = 200.0;
		HelperAPP::getSprite("pacman")->UISize.x = 32.0;
		HelperAPP::getSprite("pacman")->UISize.y = 32.0;
		Texture* pacmanTex1 = TextureLoader::LoadTexture("textures/pac_0.png");
		pacmanTex1->name = "pacmanTex1";
		HelperAPP::addTexture(pacmanTex1);
		Texture* pacmanTex2 = TextureLoader::LoadTexture("textures/pac_15.png");
		pacmanTex2->name = "pacmanTex2";
		HelperAPP::addTexture(pacmanTex2);
		Texture* pacmanTex3 = TextureLoader::LoadTexture("textures/pac_45.png");
		pacmanTex3->name = "pacmanTex3";
		HelperAPP::addTexture(pacmanTex3);
		pacman->addTexture(HelperAPP::getTexture("pacmanTex1"));
		pacman->addTexture(HelperAPP::getTexture("pacmanTex2"));
		pacman->addTexture(HelperAPP::getTexture("pacmanTex3"));
		pacman->addTexture(HelperAPP::getTexture("pacmanTex2"));
		pacman->setTextureChangeDelay(25); 
		HelperAPP::createStage("firstStage");
		HelperAPP::setStageActive("firstStage");
		HelperAPP::addSpriteToStage("firstStage","pacman"); 
	} 
}

};
#endif
