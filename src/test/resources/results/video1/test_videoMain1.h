/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_VIDEOMAIN1
#define TEST_VIDEOMAIN1
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "test_videoMain1_needsInit.h"
#include "test_videoMain1_main.h"
using namespace arma;
class test_videoMain1{
public:
double CONSTANTPORT1;
test_videoMain1_needsInit needsInit;
test_videoMain1_main main;
void init()
{
this->CONSTANTPORT1 = 0;
needsInit.init();
main.init();
}
void execute()
{
needsInit.reset = CONSTANTPORT1;
needsInit.execute();
main.needsInit = needsInit.out1;
main.execute();
}

};
#endif
