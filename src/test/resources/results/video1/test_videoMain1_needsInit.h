/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_VIDEOMAIN1_NEEDSINIT
#define TEST_VIDEOMAIN1_NEEDSINIT
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_videoMain1_needsInit{
public:
bool reset;
bool out1;
double lastVal;
void init()
{
lastVal=true;
}
void execute()
{
if((reset)){
lastVal = 1;
}
out1 = lastVal;
lastVal = 0;
}

};
#endif
