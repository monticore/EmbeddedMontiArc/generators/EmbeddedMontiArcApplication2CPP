/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_GHOST2WORLDMANAGER_PACMANWORLDHANDLER_MOVEMENTHANDLER_PACMANMOVEMENTPOSITIONCALCULATOR
#define TEST_PACMANSAMPLEGAME_MAIN_GHOST2WORLDMANAGER_PACMANWORLDHANDLER_MOVEMENTHANDLER_PACMANMOVEMENTPOSITIONCALCULATOR
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "test_pacmanSampleGame_main_ghost2WorldManager_pacmanWorldHandler_movementHandler_pacmanMovementPositionCalculator_positionUpdater.h"
#include "test_pacmanSampleGame_main_ghost2WorldManager_pacmanWorldHandler_movementHandler_pacmanMovementPositionCalculator_positionUpdaterLastDirection.h"
using namespace arma;
class test_pacmanSampleGame_main_ghost2WorldManager_pacmanWorldHandler_movementHandler_pacmanMovementPositionCalculator{
const int tilesMaxX = 17;
const int tilesMaxY = 17;
const int tileSize = 16;
public:
bool moveUp;
bool moveDown;
bool moveLeft;
bool moveRight;
bool moveUpLastDirection;
bool moveDownLastDirection;
bool moveLeftLastDirection;
bool moveRightLastDirection;
int pacmanPositionX;
int pacmanPositionY;
int pacmanNextPositionX;
int pacmanNextPositionY;
int pacmanNextPositionXLastDirection;
int pacmanNextPositionYLastDirection;
test_pacmanSampleGame_main_ghost2WorldManager_pacmanWorldHandler_movementHandler_pacmanMovementPositionCalculator_positionUpdater positionUpdater;
test_pacmanSampleGame_main_ghost2WorldManager_pacmanWorldHandler_movementHandler_pacmanMovementPositionCalculator_positionUpdaterLastDirection positionUpdaterLastDirection;
void init()
{
positionUpdater.init();
positionUpdaterLastDirection.init();
}
void execute()
{
positionUpdater.moveUp = moveUp;
positionUpdater.moveLeft = moveLeft;
positionUpdater.moveRight = moveRight;
positionUpdater.moveDown = moveDown;
positionUpdater.pacmanPositionX = pacmanPositionX;
positionUpdater.pacmanPositionY = pacmanPositionY;
positionUpdaterLastDirection.moveUp = moveUpLastDirection;
positionUpdaterLastDirection.moveLeft = moveLeftLastDirection;
positionUpdaterLastDirection.moveRight = moveRightLastDirection;
positionUpdaterLastDirection.moveDown = moveDownLastDirection;
positionUpdaterLastDirection.pacmanPositionX = pacmanPositionX;
positionUpdaterLastDirection.pacmanPositionY = pacmanPositionY;
positionUpdaterLastDirection.execute();
positionUpdater.execute();
pacmanNextPositionX = positionUpdater.newPositionX;
pacmanNextPositionY = positionUpdater.newPositionY;
pacmanNextPositionXLastDirection = positionUpdaterLastDirection.newPositionX;
pacmanNextPositionYLastDirection = positionUpdaterLastDirection.newPositionY;
}

};
#endif
