/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_INPUTSTATESAVER
#define TEST_PACMANSAMPLEGAME_MAIN_INPUTSTATESAVER
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_pacmanSampleGame_main_inputStateSaver{
public:
bool needsInit;
bool canMove;
bool canMoveLastDirection;
bool moveUpInput;
bool moveDownInput;
bool moveLeftInput;
bool moveRightInput;
bool moveDown;
bool moveUp;
bool moveRight;
bool moveLeft;
bool moveDownLastDirection;
bool moveLeftLastDirection;
bool moveRightLastDirection;
bool moveUpLastDirection;
void init()
{
}
void execute()
{
bool inputChange = 0;
if((canMove)){
moveDownLastDirection = moveDown;
moveLeftLastDirection = moveLeft;
moveRightLastDirection = moveRight;
moveUpLastDirection = moveUp;
}
if((moveUpInput)){
moveDown = 0;
moveLeft = 0;
moveRight = 0;
moveUp = 1;
}
else if((moveDownInput)){
moveDown = 1;
moveLeft = 0;
moveRight = 0;
moveUp = 0;
}
else if((moveLeftInput)){
moveDown = 0;
moveLeft = 1;
moveRight = 0;
moveUp = 0;
}
else if((moveRightInput)){
moveDown = 0;
moveLeft = 0;
moveRight = 1;
moveUp = 0;
}
else if((needsInit)){
moveUpLastDirection = 0;
moveDownLastDirection = 0;
moveLeftLastDirection = 0;
moveRightLastDirection = 0;
moveDown = 0;
moveLeft = 0;
moveRight = 0;
moveUp = 0;
}
}

};
#endif
