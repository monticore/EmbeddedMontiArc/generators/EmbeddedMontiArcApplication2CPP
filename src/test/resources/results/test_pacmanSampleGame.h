/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME
#define TEST_PACMANSAMPLEGAME
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "test_pacmanSampleGame_main.h"
using namespace arma;
class test_pacmanSampleGame{
public:
test_pacmanSampleGame_main main;
void init()
{
main.init();
}
void execute()
{
main.execute();
}

};
#endif
