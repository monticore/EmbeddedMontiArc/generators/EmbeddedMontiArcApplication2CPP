/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_PACMANWORLDMANAGER
#define TEST_PACMANSAMPLEGAME_MAIN_PACMANWORLDMANAGER
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "test_pacmanSampleGame_main_pacmanWorldManager_pacmanWorldHandler.h"
using namespace arma;
class test_pacmanSampleGame_main_pacmanWorldManager{
const int tilesMaxX = 17;
const int tilesMaxY = 17;
public:
bool needsInit;
bool moveUp;
bool moveDown;
bool moveLeft;
bool moveRight;
bool moveUpLastDirection;
bool moveDownLastDirection;
bool moveLeftLastDirection;
bool moveRightLastDirection;
int pacmanPositionX;
int pacmanPositionY;
imat world;
int pacmanNextPositionX;
int pacmanNextPositionY;
bool canMove;
bool canMoveLastDirection;
test_pacmanSampleGame_main_pacmanWorldManager_pacmanWorldHandler pacmanWorldHandler;
void init()
{
world=imat(tilesMaxX,tilesMaxY);
pacmanWorldHandler.init();
}
void execute()
{
pacmanWorldHandler.moveUp = moveUp;
pacmanWorldHandler.moveDown = moveDown;
pacmanWorldHandler.moveLeft = moveLeft;
pacmanWorldHandler.moveRight = moveRight;
pacmanWorldHandler.moveUpLastDirection = moveUpLastDirection;
pacmanWorldHandler.moveDownLastDirection = moveDownLastDirection;
pacmanWorldHandler.moveLeftLastDirection = moveLeftLastDirection;
pacmanWorldHandler.moveRightLastDirection = moveRightLastDirection;
pacmanWorldHandler.pacmanPositionX = pacmanPositionX;
pacmanWorldHandler.pacmanPositionY = pacmanPositionY;
pacmanWorldHandler.world = world;
pacmanWorldHandler.execute();
pacmanNextPositionX = pacmanWorldHandler.pacmanNextPositionX;
pacmanNextPositionY = pacmanWorldHandler.pacmanNextPositionY;
canMove = pacmanWorldHandler.canMove;
canMoveLastDirection = pacmanWorldHandler.canMoveLastDirection;
}

};
#endif
