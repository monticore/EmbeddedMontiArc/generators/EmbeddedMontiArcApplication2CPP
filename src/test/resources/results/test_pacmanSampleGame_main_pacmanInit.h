/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_PACMANINIT
#define TEST_PACMANSAMPLEGAME_MAIN_PACMANINIT
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "stdio.h"
#include "SDL.h"
#include "dcw/sys/HelperAPP.hpp"
#include "dcw/sys/TextureLoader.hpp"
#include "dcw/sys/Square2D.hpp"
using namespace arma;
class test_pacmanSampleGame_main_pacmanInit{
public:
bool exec;
bool initDone;
void init()
{
}
void execute()
{
	if (exec) 
	{ 
		HelperAPP::setWindowSize(816, 816);
		Texture* pacman1 = TextureLoader::LoadTexture("textures/pacman1.png");
		pacman1->name = "pacman1";
		HelperAPP::addTexture(pacman1);
		Texture* pacman2 = TextureLoader::LoadTexture("textures/pacman2.png");
		pacman2->name = "pacman2";
		HelperAPP::addTexture(pacman2);
		Texture* pacman3 = TextureLoader::LoadTexture("textures/pacman3.png");
		pacman3->name = "pacman3";
		HelperAPP::addTexture(pacman3);
		Square2D* pacman = new Square2D(HelperAPP::getShader());
		pacman->name = "pacman";
		HelperAPP::addSprite(pacman);
		pacman->addTexture(HelperAPP::getTexture("pacman1"));
		pacman->addTexture(HelperAPP::getTexture("pacman2"));
		pacman->addTexture(HelperAPP::getTexture("pacman3"));
		pacman->addTexture(HelperAPP::getTexture("pacman2"));
		pacman->setTextureChangeDelay(10); 
		HelperAPP::createStage("defaultStage");
		HelperAPP::addSpriteToStage("defaultStage","pacman"); 
		HelperAPP::setStageActive("defaultStage");
		HelperAPP::getSprite("pacman")->UISize.x = 48.0;
		HelperAPP::getSprite("pacman")->UISize.y = 48.0;
		Texture* texture_ghost1 = TextureLoader::LoadTexture("textures/ghost1.png");
		texture_ghost1->name = "texture_ghost1";
		HelperAPP::addTexture(texture_ghost1);
		Square2D* ghost1 = new Square2D(HelperAPP::getShader());
		ghost1->name = "ghost1";
		HelperAPP::addSprite(ghost1);
		ghost1->addTexture(HelperAPP::getTexture("texture_ghost1"));
		HelperAPP::addSpriteToStage("defaultStage","ghost1"); 
		HelperAPP::setStageActive("defaultStage");
		HelperAPP::getSprite("ghost1")->UISize.x = 48.0;
		HelperAPP::getSprite("ghost1")->UISize.y = 48.0;
		Texture* texture_ghost2 = TextureLoader::LoadTexture("textures/ghost2.png");
		texture_ghost2->name = "texture_ghost2";
		HelperAPP::addTexture(texture_ghost2);
		Square2D* ghost2 = new Square2D(HelperAPP::getShader());
		ghost2->name = "ghost2";
		HelperAPP::addSprite(ghost2);
		ghost2->addTexture(HelperAPP::getTexture("texture_ghost2"));
		HelperAPP::addSpriteToStage("defaultStage","ghost2"); 
		HelperAPP::setStageActive("defaultStage");
		HelperAPP::getSprite("ghost2")->UISize.x = 48.0;
		HelperAPP::getSprite("ghost2")->UISize.y = 48.0;
		Texture* texture_ghost3 = TextureLoader::LoadTexture("textures/ghost3.png");
		texture_ghost3->name = "texture_ghost3";
		HelperAPP::addTexture(texture_ghost3);
		Square2D* ghost3 = new Square2D(HelperAPP::getShader());
		ghost3->name = "ghost3";
		HelperAPP::addSprite(ghost3);
		ghost3->addTexture(HelperAPP::getTexture("texture_ghost3"));
		HelperAPP::addSpriteToStage("defaultStage","ghost3"); 
		HelperAPP::setStageActive("defaultStage");
		HelperAPP::getSprite("ghost3")->UISize.x = 48.0;
		HelperAPP::getSprite("ghost3")->UISize.y = 48.0;
		Texture* texture_ghost4 = TextureLoader::LoadTexture("textures/ghost4.png");
		texture_ghost4->name = "texture_ghost4";
		HelperAPP::addTexture(texture_ghost4);
		Square2D* ghost4 = new Square2D(HelperAPP::getShader());
		ghost4->name = "ghost4";
		HelperAPP::addSprite(ghost4);
		ghost4->addTexture(HelperAPP::getTexture("texture_ghost4"));
		HelperAPP::addSpriteToStage("defaultStage","ghost4"); 
		HelperAPP::setStageActive("defaultStage");
		HelperAPP::getSprite("ghost4")->UISize.x = 48.0;
		HelperAPP::getSprite("ghost4")->UISize.y = 48.0;
	} 
}

};
#endif
