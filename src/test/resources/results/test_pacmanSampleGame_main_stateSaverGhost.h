/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_STATESAVERGHOST
#define TEST_PACMANSAMPLEGAME_MAIN_STATESAVERGHOST
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
using namespace arma;
class test_pacmanSampleGame_main_stateSaverGhost{
public:
bool needsInit;
int ghost1PositionX;
int ghost1PositionY;
int ghost2PositionX;
int ghost2PositionY;
int ghost3PositionX;
int ghost3PositionY;
int ghost4PositionX;
int ghost4PositionY;
int ghost1PositionXOut;
int ghost1PositionYOut;
int ghost2PositionXOut;
int ghost2PositionYOut;
int ghost3PositionXOut;
int ghost3PositionYOut;
int ghost4PositionXOut;
int ghost4PositionYOut;
int ghost1LastPositionXOut;
int ghost1LastPositionYOut;
int ghost2LastPositionXOut;
int ghost2LastPositionYOut;
int ghost3LastPositionXOut;
int ghost3LastPositionYOut;
int ghost4LastPositionXOut;
int ghost4LastPositionYOut;
void init()
{
}
void execute()
{
ghost1LastPositionXOut = ghost1PositionX;
ghost1LastPositionYOut = ghost1PositionY;
ghost2LastPositionXOut = ghost2PositionX;
ghost2LastPositionYOut = ghost2PositionY;
ghost3LastPositionXOut = ghost3PositionX;
ghost3LastPositionYOut = ghost3PositionY;
ghost4LastPositionXOut = ghost4PositionX;
ghost4LastPositionYOut = ghost4PositionY;
if((needsInit)){
ghost1PositionXOut = 336;
ghost1PositionYOut = 336;
ghost2PositionXOut = 384;
ghost2PositionYOut = 336;
ghost3PositionXOut = 432;
ghost3PositionYOut = 336;
ghost4PositionXOut = 384;
ghost4PositionYOut = 270;
ghost1LastPositionXOut = ghost1PositionX;
ghost1LastPositionYOut = ghost1PositionY;
ghost2LastPositionXOut = ghost2PositionX;
ghost2LastPositionYOut = ghost2PositionY;
ghost3LastPositionXOut = ghost3PositionX;
ghost3LastPositionYOut = ghost3PositionY;
ghost4LastPositionXOut = ghost4PositionX;
ghost4LastPositionYOut = ghost4PositionY;
}
else {
ghost1PositionXOut = ghost1PositionX;
ghost1PositionYOut = ghost1PositionY;
ghost2PositionXOut = ghost2PositionX;
ghost2PositionYOut = ghost2PositionY;
ghost3PositionXOut = ghost3PositionX;
ghost3PositionYOut = ghost3PositionY;
ghost4PositionXOut = ghost4PositionX;
ghost4PositionYOut = ghost4PositionY;
}
}

};
#endif
