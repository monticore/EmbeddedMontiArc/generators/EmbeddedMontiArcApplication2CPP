/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_PACMANGHOSTCOLLISIONCHECKER
#define TEST_PACMANSAMPLEGAME_MAIN_PACMANGHOSTCOLLISIONCHECKER
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "test_pacmanSampleGame_main_pacmanGhostCollisionChecker_collisionChecker_1_.h"
#include "test_pacmanSampleGame_main_pacmanGhostCollisionChecker_decider.h"
using namespace arma;
class test_pacmanSampleGame_main_pacmanGhostCollisionChecker{
public:
int pacmanTilePositionX;
int pacmanTilePositionY;
int ghost1TilePositionX;
int ghost1TilePositionY;
int ghost2TilePositionX;
int ghost2TilePositionY;
int ghost3TilePositionX;
int ghost3TilePositionY;
int ghost4TilePositionX;
int ghost4TilePositionY;
bool pacmanDies;
bool ghost1Dies;
bool ghost2Dies;
bool ghost3Dies;
bool ghost4Dies;
test_pacmanSampleGame_main_pacmanGhostCollisionChecker_collisionChecker_1_ collisionChecker[4];
test_pacmanSampleGame_main_pacmanGhostCollisionChecker_decider decider;
void init()
{
collisionChecker[0].init();
collisionChecker[1].init();
collisionChecker[2].init();
collisionChecker[3].init();
decider.init();
}
void execute()
{
collisionChecker[0].position1X = pacmanTilePositionX;
collisionChecker[0].position1Y = pacmanTilePositionY;
collisionChecker[0].position2X = ghost1TilePositionX;
collisionChecker[0].position2Y = ghost1TilePositionY;
collisionChecker[0].execute();
collisionChecker[1].position1X = pacmanTilePositionX;
collisionChecker[1].position1Y = pacmanTilePositionY;
collisionChecker[1].position2X = ghost2TilePositionX;
collisionChecker[1].position2Y = ghost2TilePositionY;
collisionChecker[1].execute();
collisionChecker[2].position1X = pacmanTilePositionX;
collisionChecker[2].position1Y = pacmanTilePositionY;
collisionChecker[2].position2X = ghost3TilePositionX;
collisionChecker[2].position2Y = ghost3TilePositionY;
collisionChecker[2].execute();
collisionChecker[3].position1X = pacmanTilePositionX;
collisionChecker[3].position1Y = pacmanTilePositionY;
collisionChecker[3].position2X = ghost4TilePositionX;
collisionChecker[3].position2Y = ghost4TilePositionY;
collisionChecker[3].execute();
decider.collisionGhost1 = collisionChecker[0].collides;
decider.collisionGhost2 = collisionChecker[1].collides;
decider.collisionGhost3 = collisionChecker[2].collides;
decider.collisionGhost4 = collisionChecker[3].collides;
decider.execute();
pacmanDies = decider.pacmanDies;
ghost1Dies = decider.ghost1Dies;
ghost2Dies = decider.ghost2Dies;
ghost3Dies = decider.ghost3Dies;
ghost4Dies = decider.ghost4Dies;
}

};
#endif
