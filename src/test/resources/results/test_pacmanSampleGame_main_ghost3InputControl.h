/* (c) https://github.com/MontiCore/monticore */
#ifndef TEST_PACMANSAMPLEGAME_MAIN_GHOST3INPUTCONTROL
#define TEST_PACMANSAMPLEGAME_MAIN_GHOST3INPUTCONTROL
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <cstdlib>
#include "armadillo.h"
#include "stdio.h"
#include "SDL.h"
#include "dcw/sys/HelperAPP.hpp"
#include "dcw/sys/TextureLoader.hpp"
#include "dcw/sys/Square2D.hpp"
using namespace arma;
class test_pacmanSampleGame_main_ghost3InputControl{
public:
bool moveDown;
bool moveUp;
bool moveLeft;
bool moveRight;
bool moveDownP2;
bool moveUpP2;
bool moveLeftP2;
bool moveRightP2;
void init()
{
}
void execute()
{
	moveDown = HelperAPP::isKeyPressed(SDLK_w);
	moveUp = HelperAPP::isKeyPressed(SDLK_s);
	moveLeft = HelperAPP::isKeyPressed(SDLK_a);
	moveRight = HelperAPP::isKeyPressed(SDLK_d);
	moveDownP2 = HelperAPP::isKeyPressed(SDLK_i);
	moveUpP2 = HelperAPP::isKeyPressed(SDLK_k);
	moveLeftP2 = HelperAPP::isKeyPressed(SDLK_j);
	moveRightP2 = HelperAPP::isKeyPressed(SDLK_l);
}

};
#endif
