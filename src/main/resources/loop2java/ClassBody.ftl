<#-- (c) https://github.com/MontiCore/monticore -->
package de.monticore.lang.embeddedmontiarc.generator;

import java.math.BigInteger;

public class ${loopName} implements LoopProgram {
    <#if ast.getOutput()??><#list ast.getOutput().get().getNames() as output>
    private BigInteger ${output};
    </#list></#if>

    public ${loopName}() {
        <#if ast.getOutput()??><#list ast.getOutput().get().getNames() as output>
        ${output} = BigInteger.ZERO;
        </#list></#if>
    }

    @Override
    public int getNumInputs() {
        <#if ast.getInput()??>
        return ${ast.getInput().get().getNames()?size};
        <#else>
        return 0;
        </#if>
    }

    @Override
    public int getNumOutputs() {
        <#if ast.getOutput()??>
        return ${ast.getOutput().get().getNames()?size};
        <#else>
        return 0;
        </#if>
    }

    <#if ast.getOutput()??><#list ast.getOutput().get().getNames() as output>
    public BigInteger get${output}() {
        return ${output};
    }
    </#list></#if>

    public BigInteger[] getOutput() {
        return new BigInteger[] {
            <#if ast.getOutput()??><#list ast.getOutput().get().getNames() as output>
            ${output} <#if output_has_next>, </#if>
            </#list></#if>
        };
    }

    @Override
    public BigInteger[] run(BigInteger[] input) {
        run(
            <#if ast.getInput()??><#list ast.getInput().get().getNames() as input>
            input[${input?index}] <#if input_has_next>, </#if>
            </#list></#if>
        );
        return getOutput();
    }

    public void run(
        <#if ast.getInput()??><#list ast.getInput().get().getNames() as input>
        BigInteger ${input} <#if input_has_next>, </#if>
        </#list></#if>
    ) {
        // initialize all variables
        <#if ast.getOutput()??><#list ast.getOutput().get().getNames() as output>
        ${output} = BigInteger.ZERO;
        </#list></#if>
        <#list variableCollector.getLocalVariables() as localVar>
        BigInteger ${localVar} = BigInteger.ZERO;
        </#list>

        // run the program
        ${gen.generateCode(ast)}
    }
}
