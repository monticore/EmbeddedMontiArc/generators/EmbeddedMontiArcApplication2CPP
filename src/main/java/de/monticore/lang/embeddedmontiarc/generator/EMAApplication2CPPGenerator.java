/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarc.generator;

import de.monticore.lang.application.application._symboltable.ApplicationStatementsSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.generator.cpp.LanguageUnitEMAACPP;
import de.monticore.lang.math._symboltable.MathStatementsSymbol;
import de.monticore.lang.monticar.generator.BluePrint;
import de.monticore.lang.monticar.generator.Helper;
import de.monticore.lang.monticar.generator.cpp.BluePrintCPP;
import de.monticore.lang.monticar.generator.cpp.GeneratorCPP;
import de.monticore.lang.monticar.generator.cpp.LanguageUnitCPP;
import de.monticore.lang.tagging._symboltable.TaggingResolver;
import de.monticore.symboltable.Scope;
import de.se_rwth.commons.logging.Log;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 */
public class EMAApplication2CPPGenerator extends GeneratorCPP {
    protected final List<BluePrintCPP> bluePrints = new ArrayList<>();

    public EMAApplication2CPPGenerator() {
        super();
    }

    @Override
    public String generateString(TaggingResolver taggingResolver, ExpandedComponentInstanceSymbol componentSymbol, MathStatementsSymbol mathStatementsSymbol) {
        LanguageUnitEMAACPP languageUnitCPP = new LanguageUnitEMAACPP();
        Optional<ApplicationStatementsSymbol> appSymbol = taggingResolver.resolve(
                componentSymbol.getComponentType().getReferencedSymbol().getFullName() + ".ApplicationStatements",
                ApplicationStatementsSymbol.KIND);
        Log.debug(componentSymbol.getComponentType().getReferencedSymbol().getFullName() + ".ApplicationStatements", "generateString");
        languageUnitCPP.appStatementsSymbol = appSymbol;
        languageUnitCPP.setGeneratorCPP(this);
        languageUnitCPP.addSymbolToConvert(componentSymbol);
        if (mathStatementsSymbol != null)
            languageUnitCPP.addSymbolToConvert(mathStatementsSymbol);
        languageUnitCPP.generateBluePrints();

        BluePrintCPP bluePrintCPP = null;
        for (BluePrint bluePrint : languageUnitCPP.getBluePrints()) {
            if (bluePrint.getOriginalSymbol().equals(componentSymbol)) {
                bluePrintCPP = (BluePrintCPP) bluePrint;
            }
        }

        if (bluePrintCPP != null) {
            bluePrints.add(bluePrintCPP);
            bluePrintCPP.setGenerator(this);
        }
        languageUnitCPP.setGeneratorCPP(this);
        String result = languageUnitCPP.getGeneratedHeader(taggingResolver, bluePrintCPP);
        return result;
    }

    public List<BluePrintCPP> getBluePrints() {
        return Collections.unmodifiableList(bluePrints);
    }
}
