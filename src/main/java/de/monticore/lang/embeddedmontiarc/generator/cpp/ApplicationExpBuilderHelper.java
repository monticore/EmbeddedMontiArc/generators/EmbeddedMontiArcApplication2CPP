/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarc.generator.cpp;

import de.monticore.lang.application.application._ast.ASTApplicationExp;
import de.monticore.lang.application.application._ast.ASTApplicationNameAddExp;
import de.monticore.lang.application.application._ast.ASTApplicationNameExp;
import de.monticore.lang.application.application._ast.ASTApplicationStringExp;
import de.monticore.lang.application.application._visitor.ApplicationVisitor;

import java.util.HashMap;
import java.util.Map;

/**
 */
public class ApplicationExpBuilderHelper implements ApplicationVisitor {

    String currentString = "std::string()";
    Map<ASTApplicationExp, String> conversion = new HashMap<>();

    public static String convert(ASTApplicationExp node) {
        ApplicationExpBuilderHelper helper = new ApplicationExpBuilderHelper();

        node.accept(helper);

        return helper.currentString+helper.conversion.get(node);
    }

    @Override
    public void endVisit(ASTApplicationNameAddExp node) {
        String firstConversion = conversion.get(node.getApplicationExp(0));
        String secondConversion = conversion.get(node.getApplicationExp(1));
        conversion.put(node,""+firstConversion+"" + ""+secondConversion+"");
    }

    @Override
    public void visit(ASTApplicationStringExp node) {
        conversion.put(node, ".append(\"" + node.getString() + "\")\n");
    }

    @Override
    public void visit(ASTApplicationNameExp node) {
        conversion.put(node,".append(std::to_string(" + node.getName() + "))\n");
    }
}
