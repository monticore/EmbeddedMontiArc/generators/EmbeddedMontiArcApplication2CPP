/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarc.generator.cpp;

import de.monticore.lang.application.application._ast.*;
import de.monticore.lang.application.application._symboltable.ApplicationStatementsSymbol;
import de.monticore.lang.application.application._visitor.ApplicationVisitor;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarcapplication._ast.ASTEMAAplCompilationUnit;
import de.monticore.lang.monticar.generator.Method;
import de.monticore.lang.monticar.generator.TargetCodeInstruction;
import de.monticore.lang.monticar.generator.cpp.BluePrintCPP;
//TODO mouse button handling for input
//TODO proper cleanup of all C++ resources
public class ApplicationGenerator implements ApplicationVisitor {
    protected final String NEWLINE = "\n";
    protected final String NEWLINET = "\n\t\t";
    protected String generatedCode;
    //protected VariableCollector variableCollector;
    protected BluePrintCPP blueprint;
    protected int indentation = 1;

    String windowTitle = "EMAApl Demo";
    protected float windowSizeX = 0;
    protected float windowSizeY = 0;

    public void generateCode(ApplicationStatementsSymbol apps, BluePrintCPP blueprint) {
        this.blueprint = blueprint;
        generatedCode = "";
        initIncludes();
        //variableCollector = new VariableCollector(astcu);
        ((ASTApplicationNode) apps.getAstNode().get()).accept(this);
        Method m = new Method("execute", "void");

        TargetCodeInstruction code = new TargetCodeInstruction(generatedCode);
        m.addInstruction(code);
        blueprint.addMethod(m);

        //return generatedCode;
    }

    public void initIncludes() {
        blueprint.addAdditionalIncludeString("stdio");
        blueprint.addAdditionalIncludeString("SDL");
        blueprint.addAdditionalIncludeString("dcw/sys/HelperAPP.hpp");
        blueprint.addAdditionalIncludeString("dcw/sys/TextureLoader.hpp");
        blueprint.addAdditionalIncludeString("dcw/sys/Square2D.hpp");
    }

    @Override
    public void visit(ASTSetWindowSize node) {
        addIndent();
        if (node.getXOpt().isPresent()) {
            int w = node.getXOpt().get().getNumber().get().intValue();
            int h = node.getYOpt().get().getNumber().get().intValue();
            generatedCode += "HelperAPP::setWindowSize(" + w + ", " + h + ");\n";
        } else {
            // 0 = windowed
            // 1 = fullscreen
            // 2 = fullscreen desktopsize
            generatedCode += "HelperAPP::setWindowFullscreen(1);\n";
        }
    }

    @Override
    public void visit(ASTInputStatusCheck node) {
        addIndent();
        String keyCodeName = "SDLK_" + node.getInput().getString();
        generatedCode += node.getPortName() + " = HelperAPP::isKeyPressed(" + keyCodeName + ");\n";
    }

    @Override
    public void visit(ASTTextureLoadStatement node) {
        addIndent();
        String tname = node.getTextureName();
        String path = node.getTexturePath();
        generatedCode += "Texture* " + tname + " = TextureLoader::LoadTexture(\"" + path + "\");\n";
        addIndent();
        generatedCode += tname + "->name = \"" + tname + "\";\n";
        addIndent();
        generatedCode += "HelperAPP::addTexture(" + node.getTextureName() + ");\n";
    }

    @Override
    public void visit(ASTTextureUnLoadStatement node) {
        addIndent();
        generatedCode += "//TODO delete " + node.getTextureName() + ";\n";
    }

    @Override
    public void visit(ASTSpriteVisibleStatement node) {
        addIndent();
        String name = node.getSpriteName();
        generatedCode += name + "->visible = ";
        if (node.isVisible())
            generatedCode += "true;\n";
        else
            generatedCode += "false;\n";
    }

    @Override
    public void visit(ASTSpritePositionStatement node) {
        addIndent();
        if (node.getPortName1Opt().isPresent())
            generatedCode += "HelperAPP::getSprite(\"" + node.getSpriteName() + "\")->position.x = " + node.getPortName1() + ";\n";
        else
            generatedCode += "HelperAPP::getSprite(\"" + node.getSpriteName() + "\")->position.x = " + node.getUnitNumberResolution(0).getNumber().get() + ";\n";
        addIndent();
        if (node.getPortName2Opt().isPresent())
            generatedCode += "HelperAPP::getSprite(\"" + node.getSpriteName() + "\")->position.y = " + node.getPortName2() + ";\n";
        else
            generatedCode += "HelperAPP::getSprite(\"" + node.getSpriteName() + "\")->position.y = " + node.getUnitNumberResolution(1).getNumber().get() + ";\n";
    }

    @Override
    public void visit(ASTSpriteSizeStatement node) {
        addIndent();
        if (node.getPortName1Opt().isPresent())
            generatedCode += "HelperAPP::getSprite(\"" + node.getSpriteName() + "\")->UISize.x = " + node.getPortName1() + ";\n";
        else
            generatedCode += "HelperAPP::getSprite(\"" + node.getSpriteName() + "\")->UISize.x = " + node.getUnitNumberResolution(0).getNumber().get() + ";\n";
        addIndent();
        if (node.getPortName2Opt().isPresent())
            generatedCode += "HelperAPP::getSprite(\"" + node.getSpriteName() + "\")->UISize.y = " + node.getPortName2() + ";\n";
        else
            generatedCode += "HelperAPP::getSprite(\"" + node.getSpriteName() + "\")->UISize.y = " + node.getUnitNumberResolution(1).getNumber().get() + ";\n";

    }

    @Override
    public void visit(ASTSpriteStageStatement node) {
        addIndent();
        generatedCode += "HelperAPP::addSpriteToStage(\"" + node.getStageName() + "\",\"" + node.getSpriteName() + "\"); \n";
    }

    @Override
    public void visit(ASTSpriteDeleteStatement node) {
        addIndent();
        if (node.getApplicationExpOpt().isPresent()) {
            generatedCode+="HelperAPP::removeSprite("+ApplicationExpBuilderHelper.convert(node.getApplicationExp())+");\n";
        } else
            generatedCode += "HelperAPP::removeSprite(\"" + node.getSpriteName() + "\"); \n";
    }

    @Override
    public void visit(ASTSpriteCreateStatement node) {
        addIndent();
        String name = node.getSpriteName();
        generatedCode += "Square2D* " + name + " = new Square2D(HelperAPP::getShader());\n";
        addIndent();
        generatedCode += name + "->name = \"" + name + "\";\n";
        addIndent();
        generatedCode += "HelperAPP::addSprite(" + name + ");\n";
    }

    @Override
    public void visit(ASTSpriteAddTexture node) {
        addIndent();
        String sname = node.getSpriteName();
        String tname = node.getTextureName();
        generatedCode += sname + "->addTexture(HelperAPP::getTexture(\"" + tname + "\"));\n";
    }

    @Override
    public void visit(ASTSpriteRemoveTexture node) {
        addIndent();
        generatedCode += "//TODO SpriteRemoveTexture \n";
    }

    @Override
    public void visit(ASTSpriteNextTexture node) {
        addIndent();
        String sname = node.getSpriteName();
        int ticks = node.getTicks().getNumber().get().intValue();
        generatedCode += sname + "->setTextureChangeDelay(" + ticks + "); \n";
    }

    @Override
    public void visit(ASTActiveStageStatement node) {
        addIndent();
        if (node.isActive()) {
            generatedCode += "HelperAPP::setStageActive(\"" + node.getStageName() + "\");\n";
        } else {
            generatedCode += "HelperAPP::setStageInActive(\"" + node.getStageName() + "\");\n";
        }
    }

    @Override
    public void visit(ASTHideStagesStatement node) {
        addIndent();
        generatedCode += "//TODO ASTHideStagesStatement \n";
    }

    @Override
    public void visit(ASTCreateStageStatement node) {
        addIndent();
        generatedCode += "HelperAPP::createStage(\"" + node.getStageName() + "\");\n";
    }

    @Override
    public void visit(ASTDeleteStageStatement node) {
        addIndent();
        generatedCode += "//TODO ASTDeleteStageStatement \n";
    }

    @Override
    public void visit(ASTSimpleIf node) {
        addIndent();
        String name = node.getPortName();
        generatedCode += "if (" + name + ") \n";
        addIndent();
        generatedCode += "{ \n";
        indentation++;
    }

    @Override
    public void endVisit(ASTSimpleIf node) {
        indentation--;
        addIndent();
        generatedCode += "} \n";
    }

    protected void addIndent() {
        for (int i = 0; i < indentation; i++)
            generatedCode += "\t";
    }

}
