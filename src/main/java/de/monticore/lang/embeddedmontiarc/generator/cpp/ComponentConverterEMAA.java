/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarc.generator.cpp;

import de.monticore.lang.application.application._symboltable.ApplicationStatementsSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.generator.BluePrint;
import de.monticore.lang.monticar.generator.Method;
import de.monticore.lang.monticar.generator.cpp.*;
import de.monticore.lang.monticar.generator.cpp.converter.ComponentConverter;
import de.monticore.lang.monticar.generator.cpp.converter.ComponentConverterMethodGeneration;
import de.monticore.lang.monticar.generator.cpp.converter.ComponentInstanceConverter;
import de.se_rwth.commons.logging.Log;

import java.util.List;

import static de.monticore.lang.monticar.generator.cpp.converter.ComponentConverter.addVariables;
import static de.monticore.lang.monticar.generator.cpp.converter.ComponentConverter.generateInitMethod;

/**
 */
public class ComponentConverterEMAA {

    //TODO change this method to generate appl behaviour
    public static BluePrint convertComponentSymbolToBluePrint(ExpandedComponentInstanceSymbol componentSymbol,
                                                              ApplicationStatementsSymbol appStatementsSymbol,
                                                              List<String> includeStrings, GeneratorCPP generatorCPP) {
        BluePrintCPP bluePrint = new BluePrintCPP(GeneralHelperMethods.getTargetLanguageComponentName(componentSymbol.getFullName()));
        ComponentConverter.currentBluePrint = bluePrint;
        bluePrint.setGenerator(generatorCPP);
        bluePrint.setOriginalSymbol(componentSymbol);
        bluePrint.addDefineGenerics(componentSymbol);

        addVariables(componentSymbol, bluePrint);

        String lastNameWithoutArrayPart = "";
        for (ExpandedComponentInstanceSymbol instanceSymbol : componentSymbol.getSubComponents()) {
            int arrayBracketIndex = instanceSymbol.getName().indexOf("[");
            boolean generateComponentInstance = true;
            if (arrayBracketIndex != -1) {
                generateComponentInstance = !instanceSymbol.getName().substring(0, arrayBracketIndex).equals(
                        lastNameWithoutArrayPart);
                lastNameWithoutArrayPart = instanceSymbol.getName().substring(0, arrayBracketIndex);
                Log.info(lastNameWithoutArrayPart, "Without:");
                Log.info(generateComponentInstance + "", "Bool:");
            }
            if (generateComponentInstance) {
            }
            bluePrint.addVariable(ComponentInstanceConverter.convertComponentInstanceSymbolToVariable(instanceSymbol,
                    componentSymbol));
        }

        //create arrays from variables that only differ at the end by _number_
        BluePrintFixer.fixBluePrintVariableArrays(bluePrint);

        generateInitMethod(componentSymbol, bluePrint, generatorCPP, includeStrings);


        //generate execute method
        ComponentConverterMethodGeneration.generateExecuteMethod(componentSymbol, bluePrint, null,
                generatorCPP, includeStrings);
        handleApplicationStatementGeneration(bluePrint.getMethods().get(bluePrint.getMethods().size() - 1), bluePrint,
                appStatementsSymbol, generatorCPP, includeStrings);

        return bluePrint;
    }

    private static void handleApplicationStatementGeneration(Method method, BluePrintCPP bluePrint,
                                                             ApplicationStatementsSymbol appStatementsSymbol, GeneratorCPP generatorCPP, List<String> includeStrings) {
        // add math implementation instructions to method
        /*List<MathExpressionSymbol> newMathExpressionSymbols = new ArrayList<>();
        MathOptimizer.currentBluePrint = bluePrint;
        int counter = 0;
        visitedMathExpressionSymbols.clear();
        //int lastIndex = 0;
        for (currentGenerationIndex = 0; currentGenerationIndex < mathStatementsSymbol.getMathExpressionSymbols().size(); ++currentGenerationIndex) {
            int beginIndex = currentGenerationIndex;
            MathExpressionSymbol mathExpressionSymbol = mathStatementsSymbol.getMathExpressionSymbols().get(currentGenerationIndex);
            if (!visitedMathExpressionSymbols.contains(mathExpressionSymbol)) {
                if (generatorCPP.useAlgebraicOptimizations()) {
                    List<MathExpressionSymbol> precedingExpressions = new ArrayList<>();
                    for (int i = 0; i < counter; ++i)
                        precedingExpressions.add(mathStatementsSymbol.getMathExpressionSymbols().get(i));
                    if (mathExpressionSymbol != visitedMathExpressionSymbols)
                        newMathExpressionSymbols.add(MathOptimizer.applyOptimizations(mathExpressionSymbol, precedingExpressions, mathStatementsSymbol));
                    ++counter;
                }
                generateInstruction(method, mathExpressionSymbol, bluePrint, includeStrings);
                //lastIndex = currentGenerationIndex;
            }
            handleInstructionReOrdering(method, beginIndex);
        }
        if (generatorCPP.useAlgebraicOptimizations())
            removeUselessVariableDefinitions(method);*/

        ApplicationGenerator appGen = new ApplicationGenerator();
        appGen.generateCode(appStatementsSymbol, bluePrint);

    }
}
