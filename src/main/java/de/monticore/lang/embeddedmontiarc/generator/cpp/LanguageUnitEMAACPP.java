/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarc.generator.cpp;

import de.monticore.lang.application.application._symboltable.ApplicationStatementsSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.math._symboltable.MathStatementsSymbol;
import de.monticore.lang.monticar.generator.*;
import de.monticore.lang.monticar.generator.cpp.BluePrintCPP;
import de.monticore.lang.monticar.generator.cpp.ExecutionOrderFixer;
import de.monticore.lang.monticar.generator.cpp.GeneratorCPP;
import de.monticore.lang.monticar.generator.cpp.LanguageUnitCPP;
import de.monticore.lang.monticar.generator.cpp.converter.ComponentConverter;
import de.monticore.lang.monticar.generator.cpp.converter.MathConverter;
import de.monticore.lang.monticar.generator.cpp.instruction.ConnectInstructionCPP;
import de.monticore.lang.tagging._symboltable.TaggingResolver;
import de.monticore.symboltable.Symbol;
import de.se_rwth.commons.logging.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 */
public class LanguageUnitEMAACPP extends LanguageUnitCPP {
    List<String> includeStrings = new ArrayList<>();

    GeneratorCPP generatorCPP;
    public Optional<ApplicationStatementsSymbol> appStatementsSymbol = Optional.empty();

    public LanguageUnitEMAACPP() {
        super();
    }

    public void setGeneratorCPP(GeneratorCPP generatorCPP) {
        super.setGeneratorCPP(generatorCPP);
        this.generatorCPP = generatorCPP;
    }

    @Override
    public void generateBluePrints() {
        if (appStatementsSymbol.isPresent())
            symbolsToConvert.add(appStatementsSymbol.get());
        for (int i = 0; i < symbolsToConvert.size(); ++i) {
            Symbol symbol = symbolsToConvert.get(i);
            //only works with ExpandedComponentSymbols and MathStatementsSymbol

            if (symbol.isKindOf(ExpandedComponentInstanceSymbol.KIND)) {
                Log.info(symbol.toString(), "Current Symbol(size:" + symbolsToConvert.size() + ")" + ":");
                if (i + 1 < symbolsToConvert.size()) {
                    Symbol nextSymbol = symbolsToConvert.get(i + 1);
                    Log.info(nextSymbol.toString(), "Next Symbol:");
                    if (nextSymbol.isKindOf(MathStatementsSymbol.KIND)) {
                        BluePrint bluePrint = ComponentConverter.convertComponentSymbolToBluePrint((ExpandedComponentInstanceSymbol) symbol, (MathStatementsSymbol) nextSymbol, includeStrings, generatorCPP);
                        bluePrints.add(bluePrint);
                        ++i;
                    } else {
                        BluePrint bluePrint = ComponentConverterEMAA.convertComponentSymbolToBluePrint((ExpandedComponentInstanceSymbol) symbol, (ApplicationStatementsSymbol) nextSymbol, includeStrings, generatorCPP);
                        bluePrints.add(bluePrint);
                        ++i;
                    }

                } else {
                    BluePrint bluePrint = ComponentConverter.convertComponentSymbolToBluePrint((ExpandedComponentInstanceSymbol) symbol, includeStrings, generatorCPP);
                    bluePrints.add(bluePrint);
                }
            }

        }
    }

    @Override
    public String getGeneratedHeader(TaggingResolver taggingResolver, BluePrintCPP bluePrint) {
        //fix method duplication
        List<Method> methods = new ArrayList<>();
        List<Method> methodsToRemove = new ArrayList<>();
        for (int i = 0; i < bluePrint.getMethods().size(); ++i) {
            Method method = bluePrint.getMethods().get(i);
            boolean duplicate = false;
            for (int k = i + 1; k < bluePrint.getMethods().size(); ++k) {
                Method method2 = bluePrint.getMethods().get(k);
                if (method.getParameters().size() == 0 && method.getName().equals(method2.getName()) && method2.getParameters().size() == 0) {
                    duplicate = true;

                    methodsToRemove.add(method2);
                    for (Instruction instruction : method2.getInstructions()) {
                        method.addInstruction(instruction);
                    }

                }
            }
            if (!methodsToRemove.contains(method)) {
                methods.add(method);
            }
        }
        methods.removeAll(methodsToRemove);
        bluePrint.getMethods().clear();
        bluePrint.getMethods().addAll(methods);


        ExecutionOrderFixer.fixExecutionOrder(taggingResolver, bluePrint, (GeneratorCPP) bluePrint.getGenerator());
        String resultString = "";
        //guard defines
        resultString += "#ifndef " + bluePrint.getName().toUpperCase() + "\n";
        resultString += "#define " + bluePrint.getName().toUpperCase() + "\n";
        if (generatorCPP.useMPIDefinitionFix())
            resultString += "#ifndef M_PI\n" +
                    "#define M_PI 3.14159265358979323846\n" +
                    "#endif\n";

        List<String> alreadyGeneratedIncludes = new ArrayList<>();
        //includes
        //add default include
        resultString+= "#include <cstdlib>\n";
        if (MathConverter.curBackend.getBackendName().equals("OctaveBackend")) {
            resultString += "#include \"octave/oct.h\"\n";
            alreadyGeneratedIncludes.add("octave/oct");
        } else if (MathConverter.curBackend.getBackendName().equals("ArmadilloBackend")) {
            resultString += "#include \"" + MathConverter.curBackend.getIncludeHeaderName() + ".h\"\n";
            alreadyGeneratedIncludes.add(MathConverter.curBackend.getIncludeHeaderName());
        }
        for (Variable v : bluePrint.getVariables()) {
            //TODO remove multiple same includes
            if (v.hasInclude()) {
                if (!alreadyGeneratedIncludes.contains(v.getVariableType().getIncludeName())) {
                    alreadyGeneratedIncludes.add(v.getVariableType().getIncludeName());
                    resultString += "#include \"" + v.getVariableType().getIncludeName() + ".h\"\n";
                }
            }
        }

        for (String string : bluePrint.getAdditionalIncludeStrings()) {
            if (string.endsWith(".hpp")) {
                resultString += "#include \"" + string + "\"\n";
            } else
                resultString += "#include \"" + string + ".h\"\n";
        }

        for (String include : includeStrings) {
            resultString += include;
        }
        if (generatorCPP.useThreadingOptimizations()) {
            //if(MathConverter.curBackend.getBackendName().equals("OctaveBackend"))
            //resultString+="#include \"mingw.thread.h\"\n";
            //else if(MathConverter.curBackend.getBackendName().equals("ArmadilloBackend"))
            resultString += "#include <thread>\n";
        }
        if (MathConverter.curBackend.getBackendName().equals("ArmadilloBackend")) {
            resultString += "using namespace arma;\n";
        }

        //class definition start
        resultString += "class " + bluePrint.getName() + "{\n";

        //const variables
        for (String constString : bluePrint.getConsts())
            resultString += constString;
        resultString += "public:\n";

        //input variable
        for (Variable v : bluePrint.getVariables()) {
            if (!v.isArray())
                resultString += v.getVariableType().getTypeNameTargetLanguage() + " " + v.getNameTargetLanguageFormat() + ";\n";
            else
                resultString += v.getVariableType().getTypeNameTargetLanguage() + " " + v.getNameTargetLanguageFormat() + "[" + v.getArraySize() + "]" + ";\n";
        }

        //generate methods
        for (Method method : bluePrint.getMethods()) {
            int counter = 0;
            resultString += method.getReturnTypeName() + " " + method.getName() + "(";
            for (Variable param : method.getParameters()) {
                if (counter == 0) {
                    ++counter;
                    resultString += param.getVariableType().getTypeNameTargetLanguage() + " " + param.getNameTargetLanguageFormat();
                } else {
                    resultString += ", " + param.getVariableType().getTypeNameTargetLanguage() + " " + param.getNameTargetLanguageFormat();
                }
                if (param.isArray())
                    resultString += "[" + param.getArraySize() + "]";
            }
            resultString += ")\n";//TODO add semicolon when using source files

            //method body start
            resultString += "{\n";

            for (Instruction instruction : method.getInstructions()) {
                if (instruction instanceof ConnectInstructionCPP) {
                    ConnectInstructionCPP connectInstructionCPP = (ConnectInstructionCPP) instruction;
                    Log.info("v1: " + connectInstructionCPP.getVariable1().getName() + "v2: " + connectInstructionCPP.getVariable2().getName(), "Instruction:");
                } else if (instruction instanceof ExecuteInstruction) {
                    ExecuteInstruction executeInstruction = (ExecuteInstruction) instruction;

                }
                Log.info(resultString, "beforRes:");
                resultString += instruction.getTargetLanguageInstruction();
                Log.info(resultString, "afterRes:");
            }

            //method body end
            resultString += "}\n";
        }


        resultString += "\n";
        //class definition end
        resultString += "};\n";

        //guard define end
        resultString += "#endif\n";
        Log.info(resultString, "Before RESSSS:");
        return resultString;
    }
}
