/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.embeddedmontiarc.generator;

/**
 */
public class MapGenerator {

    public static String generateTileSprites(String spriteBaseName, String textureName, String stageName, int xStart, int xEnd,
                                     int yStart, int yEnd, int sizeX, int sizeY) {
        StringBuilder builder = new StringBuilder();
        for (int i = xStart; i < xEnd; ++i) {
            for (int j = yStart; j < yEnd; ++j) {
                builder.append(generateTileSprite(spriteBaseName + "X" + i + "Y" + j, i, j, textureName, sizeX, sizeY, stageName));
                builder.append("\n");
            }
        }
        return builder.toString();
    }

    public static String generateTileSprite(String spriteName, int x, int y, String textureName, int sizeX, int sizeY, String stageName) {
        StringBuilder builder = new StringBuilder();
        builder.append("create sprite ");
        builder.append(spriteName);
        builder.append(";\n");
        builder.append("set ");
        builder.append(spriteName);
        builder.append(" position to ");
        builder.append((x) * sizeX);
        builder.append(" ");
        builder.append((y) * sizeY);
        builder.append(";\n");
        builder.append("add texture ");
        builder.append(textureName);
        builder.append(" to ");
        builder.append(spriteName);
        builder.append(";\n");
        builder.append("set ");
        builder.append(spriteName);
        builder.append(" size to ");
        builder.append(sizeX);
        builder.append(" ");
        builder.append(sizeY);
        builder.append(";\n");
        builder.append("add sprite ");
        builder.append(spriteName);
        builder.append(" to stage ");
        builder.append(stageName);
        builder.append(";\n");
        return builder.toString();
    }
}
